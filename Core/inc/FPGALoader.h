/**
  ******************************************************************************
  * @file    Core/inc/FPGALoader.h
  * @author  Владимир Мешков <vld.meshkov@gmail.com>
  * @version V1.0.0
  * @date    10 декабря 2018
  * @brief   Класс конфигурирования FPGA
  ******************************************************************************
  * @attention
  * Всем очко!!
  *
  * <h2><center>&copy; 2018 ООО "РПС"</center></h2>
  ******************************************************************************
  */

#ifndef CORE_INC_FPGALOADER_H_
#define CORE_INC_FPGALOADER_H_

#include <FreeRTOS.h>
#include <task.h>
#include <queue.h>
#include <semphr.h>
#include <common.h>
#include "spidrv.h"
#include "rpsproto.pb.h"


class FPGALoader : public TaskBase {
public:
    static FPGALoader &Instance() {
        static FPGALoader s;
        return s;
    }

    void task();
    static void task_fpgaloader(void *param) {
        static_cast<FPGALoader *>(param)->task();
        while (1)
            vTaskDelay(portMAX_DELAY);
    }
    void LinkSpiDrv(SPIDrv *drv) {m_pSpiDrv = drv;}

    QueueHandle_t m_xQueueRGB = NULL;
    QueueHandle_t m_xQueueCommands = NULL;
    QueueHandle_t m_xQueueResponce = NULL;
    QueueSetHandle_t xEventHandle = NULL;
    SemaphoreHandle_t m_xSemaphoreIrq = NULL;

    bool GetProximity(Proximity *prox);

private:
    SPIDrv *m_pSpiDrv = NULL;

    FPGALoader();
    ~FPGALoader() {}
    FPGALoader(FPGALoader const &) = delete;
    FPGALoader& operator= (FPGALoader const &) = delete;

    Proximity m_xProximity;

    QueueHandle_t xMutex = NULL;
    void PrintCommand(const FPGAConfig *config);
    void SetDefaultConfigLz(uint8_t *address);

    bool configured = false;

};

#ifdef __cplusplus
extern "C" {
#endif

void EXTI9_5_IRQHandler();

#ifdef __cplusplus
}
#endif

#endif /* CORE_INC_FPGALOADER_H_ */
