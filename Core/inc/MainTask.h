/**
  ******************************************************************************
  * @file    Core/inc/MainTask.h
  * @author  Владимир Мешков <vld.meshkov@gmail.com>
  * @version V1.0.0
  * @date    10 декабря 2018
  * @brief   Центральный Task
  ******************************************************************************
  * @attention
  * Всем очко!!
  *
  * <h2><center>&copy; 2018 ООО "РПС"</center></h2>
  ******************************************************************************
  */

#ifndef CORE_INC_MAINTASK_H_
#define CORE_INC_MAINTASK_H_

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <queue.h>
#include <timers.h>
#include <common.h>
#include <spidrv.h>
#include "usb_device.h"
#include "usbd_hid.h"


class MainTask: public TaskBase {
public:
    static MainTask &Instance() {
        static MainTask s;
        return s;
    }

    void task();
    static void task_main(void *param) {
        static_cast<MainTask *>(param)->task();
        while (1)
            vTaskDelay(portMAX_DELAY);
    }

    void run();

    uint8_t m_vBulkInBuffer[USB_BULK_TRANSFER_SIZE];
    uint8_t m_vBulkOutBuffer[USB_BULK_TRANSFER_SIZE];

    void USBReceived();

private:
    MainTask();
    ~MainTask() {}
    MainTask(MainTask const &) = delete;
    MainTask& operator= (MainTask const &) = delete;

    void TestECC();
    void PrintBuffer(const uint8_t *buffer, uint8_t len);
    void SendBulkResponce(uint8_t cmd, uint8_t resp);

    SPIDrv *spiDrv;

private:
    USBD_HandleTypeDef *usb_dev = NULL;

    QueueSetHandle_t xEventHandle = NULL;

    TimerHandle_t m_xTimer = NULL;
    SemaphoreHandle_t m_xSemaphoreTimer = NULL;
    SemaphoreHandle_t m_xSemaphoreUsbRx = NULL;

};

#endif /* CORE_INC_MAINTASK_H_ */
