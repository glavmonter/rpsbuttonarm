/**
  ******************************************************************************
  * @file    Core/inc/Mifare.h
  * @author  Владимир Мешков <vld.meshkov@gmail.com>
  * @version V1.0.0
  * @date    19 декабря 2018
  * @brief   Класс чтения Mifare
  ******************************************************************************
  * @attention
  * Всем очко!!
  *
  * <h2><center>&copy; 2018 ООО "РПС"</center></h2>
  ******************************************************************************
  */

#ifndef CORE_INC_MIFARE_H_
#define CORE_INC_MIFARE_H_

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <queue.h>
#include <timers.h>
#include <common.h>
#include <pb_decode.h>
#include <pb_encode.h>
#include "hardware.h"
#include "stm32l4xx_ll_spi.h"
#include "mfrc630_def.h"
#include "rpsproto.pb.h"


#define MIFARE_MAX_STRING_LEN		128
#define MIFARE_MAX_UID_LEN			10

class Mifare: public TaskBase {
public:
	enum DMA {
		DMA_ENABLE,
		DMA_DISABLE
	};

	static Mifare &Instance() {
		static Mifare s;
		return s;
	}

	void task();
	static void task_mifare(void *param) {
		static_cast<Mifare *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

	QueueHandle_t m_xQueueCommand = NULL;
	QueueHandle_t m_xQueueResponce = NULL;

	bool GetMifareCard(uint8_t *uid, uint8_t *uid_size, uint32_t *atqa);

private:
	QueueHandle_t xMutex = NULL;

	uint8_t m_vUID_size = 0;
	uint8_t m_vUID[MIFARE_MAX_UID_LEN];
	uint16_t m_iATQA;

	uint8_t transfer_buffer[USB_BULK_TRANSFER_SIZE];

	MifareCommand m_xMifareCommand;
	MifareResponce m_xMifareResponce;

	/**
	 * @brief Проверка ключа
	 * @param [in] uid
	 * @param [in] uid_size
	 * @param [in] keyA
	 * @param [in] keyB
	 * @param [in] block
	 * @param [in] timeout
	 *
	 * @return
	 */
	MifareResponce_MFStatus CommandVerifyKey( const uint8_t *uid, uint8_t uid_size,
			                                  const uint8_t *keyA, const uint8_t *keyB,
							                  uint8_t block, TickType_t timeout);

	MifareResponce_MFStatus CommandReadBlock( const uint8_t *uid, uint8_t uid_size,
			                                  const uint8_t *keyA, const uint8_t *keyB,
							                  uint8_t block, uint8_t *block_data, TickType_t timeout);

	MifareResponce_MFStatus CommandWriteBlock( const uint8_t *uid, uint8_t uid_size,
			                                   const uint8_t *keyA, const uint8_t *keyB,
								               uint8_t block, uint8_t *block_data, TickType_t timeout);

	inline void PowerUp()   	{ MFRC630_PD(PIN_RESET); }
	inline void PowerDown() 	{ MFRC630_PD(PIN_SET); }
	inline void SPI_SELECT()	{ MFRC630_NSS(PIN_RESET); }
	inline void SPI_UNSELECT()	{ MFRC630_NSS(PIN_SET); }


	bool SPI_TransmitReceiveData(const uint8_t *pTxData, uint8_t *pRxData, uint16_t Size, DMA dma, uint32_t Timeout);
	bool WaitOnFlagUntilTimeout(uint32_t Flag, FlagStatus Status, uint32_t Timeout);

	void WriteRegs(uint8_t reg, const uint8_t *values, uint8_t len);
	void WriteReg(uint8_t reg, uint8_t value);

	uint8_t ReadReg(uint8_t reg);

	//****************************************************************
	uint16_t ISO14443A_REQA();
	uint16_t ISO14443A_WUPA();
	uint16_t ISO14443A_WUPA_REQA(uint8_t instruction);
	uint8_t ISO14443A_Select(uint8_t *uid, uint8_t *sak);

	//****************************************************************
	inline void MFRC630_Cmd_Idle() { WriteReg(MFRC630_REG_COMMAND, MFRC630_CMD_IDLE); }
	inline void MFRC630_Cmd_Transceive(const uint8_t *data, uint16_t len) {
		MFRC630_Cmd_Idle();
		MFRC630_Flush_Fifo();
		MFRC630_Write_Fifo(data, len);
		WriteReg(MFRC630_REG_COMMAND, MFRC630_CMD_TRANSCEIVE);
	}

	inline void MFRC630_Flush_Fifo() { WriteReg(MFRC630_REG_FIFOCONTROL, 1 << 4); }
	inline uint8_t MFRC630_Fifo_Length() { return ReadReg(MFRC630_REG_FIFOLENGTH); }
	void MFRC630_Write_Fifo(const uint8_t *data, uint16_t len);
	void MFRC630_Read_Fifo(uint8_t *rx, uint16_t len);


	inline void MFRC630_Clear_Irq0() { WriteReg(MFRC630_REG_IRQ0, (uint8_t)~(1 << 7)); }
	inline void MFRC630_Clear_Irq1() { WriteReg(MFRC630_REG_IRQ1, (uint8_t)~(1 << 7)); }
	inline uint8_t MFRC630_Irq0() { return ReadReg(MFRC630_REG_IRQ0); }
	inline uint8_t MFRC630_Irq1() { return ReadReg(MFRC630_REG_IRQ1); }

	inline void MFRC630_Timer_Set_Control(uint8_t timer, uint8_t value) { WriteReg(MFRC630_REG_T0CONTROL + (5 * timer), value); }
	inline void MFRC630_Timer_Set_Reload(uint8_t timer, uint16_t value) {
		WriteReg(MFRC630_REG_T0RELOADHI + (5 * timer), value >> 8);
		WriteReg(MFRC630_REG_T0RELOADLO + (5 * timer), 0xFF);
	}
	inline void MFRC630_Timer_Set_Value(uint8_t timer, uint16_t value) {
		WriteReg(MFRC630_REG_T0COUNTERVALHI + (5 * timer), value >> 8);
		WriteReg(MFRC630_REG_T0COUNTERVALLO + (5 * timer), 0xFF);
	}
	inline uint16_t MFRC630_Timer_Get_Value(uint8_t timer) {
		uint16_t res = ReadReg(MFRC630_REG_T0COUNTERVALHI + (5 * timer)) << 8;
		res += ReadReg(MFRC630_REG_T0COUNTERVALLO + (5 * timer));
		return res;
	}

	void MFRC630_Cmd_Auth(uint8_t key_type, uint8_t block_address, const uint8_t *card_uid);
	void MFRC630_Cmd_Load_Key(const uint8_t *key);

	uint8_t MFRC630_MF_Auth(const uint8_t *uid, uint8_t key_type, uint8_t block);
	inline void MFRC630_MF_DeAuth() { WriteReg(MFRC630_REG_STATUS, 0); }
	uint8_t MFRC630_MF_Read_Block(uint8_t block_address, uint8_t* dest);
	uint8_t MFRC630_MF_Write_Block(uint8_t block_address, const uint8_t *source);

private:
	Mifare();
	~Mifare() {}
	Mifare(Mifare const &) = delete;
	Mifare& operator= (Mifare const &) = delete;

	QueueSetHandle_t xEventHandle = NULL;

	TimerHandle_t m_xTimer = NULL;
	SemaphoreHandle_t m_xSemaphoreTimer = NULL;

private:
	SPI_TypeDef *m_pSPI;
	void ConfigureHardware();
	void PrintBuffer(const uint8_t *buffer, uint8_t len);

	void PrintSector(const uint8_t *uid, uint8_t sector, const uint8_t *keyA, const uint8_t *keyB);
	void PrintBlock(const uint8_t *uid, uint8_t block_num, const uint8_t *keyA, const uint8_t *keyB, uint8_t len);

	char m_vString[MIFARE_MAX_STRING_LEN];
};

#endif /* CORE_INC_MIFARE_H_ */
