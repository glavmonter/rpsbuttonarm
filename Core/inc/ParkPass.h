/**
  ******************************************************************************
  * @file    Core/inc/ParkPass.h
  * @author  Владимир Мешков <vld.meshkov@gmail.com>
  * @version V1.0.0
  * @date    19 декабря 2018
  * @brief   Класс для работы со считывателем ParkPass
  ******************************************************************************
  * @attention
  * Всем очко!!
  *
  * <h2><center>&copy; 2019 ООО "РПС"</center></h2>
  ******************************************************************************
  */

#ifndef CORE_INC_PARKPASS_H_
#define CORE_INC_PARKPASS_H_

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <queue.h>
#include <timers.h>
#include <common.h>
#include <pb_decode.h>
#include <pb_encode.h>
#include "hardware.h"
#include "rpsproto.pb.h"


class ParkPass: public TaskBase {
public:
	static ParkPass &Instance() {
		static ParkPass s;
		return s;
	}

	void task();
	static void task_parkpass(void *param) {
		static_cast<ParkPass *>(param)->task();
		while (1)
			vTaskDelay(portMAX_DELAY);
	}

	QueueHandle_t m_xQueueCommand = NULL;
	QueueHandle_t m_xQueueResponce = NULL;

private:
	ParkPass();
	~ParkPass() {}
	ParkPass(ParkPass const &) = delete;
	ParkPass& operator= (ParkPass const &) = delete;

	QueueSetHandle_t xEventHandle = NULL;

	TimerHandle_t m_xTimer = NULL;
	SemaphoreHandle_t m_xSemaphoreTimer = NULL;


private:
	uint8_t transfer_buffer[USB_BULK_TRANSFER_SIZE];
	BLECommand m_xBleCommand;
	BLEResponce m_xBleResponce;
};


#endif /* CORE_INC_PARKPASS_H_ */
