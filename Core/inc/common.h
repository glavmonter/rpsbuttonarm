/**
  ******************************************************************************
  * @file    Core/inc/common.h
  * @author  Владимир Мешков <vld.meshkov@gmail.com>
  * @version V1.0.0
  * @date    5 декабря 2018
  * @brief   Общие классы и функции
  ******************************************************************************
  * @attention
  * Всем очко!!
  *
  * <h2><center>&copy; 2018 ООО "РПС"</center></h2>
  ******************************************************************************
  */


#ifndef COMMON_H_
#define COMMON_H_

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <new>
#include <stm32l4xx.h>
#include "usbd_hid.h"


#define USB_BULK_TRANSFER_SIZE			64
#if (HID_EP2IN_SIZE != HID_EP2OUT_SIZE) || (USB_BULK_TRANSFER_SIZE != HID_EP2IN_SIZE)
#warning "HID_EP2IN_SIZE must be equivalent to HID_EP2OUT_SIZE and USB_BULK_TRANSFER_SIZE"
#endif



#define USB_BULK_CMD_BLE		        0x10
#define USB_BULK_CMD_RGB				0x11
#define USB_BULK_CMD_FPGA				0x12
#define USB_BULK_CMD_MIFARE				0x20
#define USB_BULK_CMD_DFU				0xA5



class TaskBase {
public:
	TaskHandle_t handle;
	virtual ~TaskBase() {
#if INCLUDE_vTaskDelete
		vTaskDelete(handle);
#endif
		return;
	}
};

template<typename T> T min(T a, T b) {
	return (a < b) ? a : b;
}

template<typename T> T max(T a, T b) {
	return (a > b) ? a : b;
}


#define RAMFUNC             __attribute__((section(".ramfunc")))
#define CONFIG_VERSION      __attribute__((section(".config.VERSION_NUMBER")))
#define CONFIG_CRC          __attribute__((section(".config.CRC")))
#define CONFIG_BUILD_ID     __attribute__((section(".config.BUILD_ID")))


template <uint32_t SIZE, class DATA_T = int8_t>
class RingBuffer {

public:
	typedef uint16_t INDEX_T;

	RingBuffer() {
			assert_param((SIZE & (SIZE - 1)) == 0);
		}

private:
	DATA_T _data[SIZE];

	__IO INDEX_T _readCount;
	__IO INDEX_T _writeCount;

	static const INDEX_T _mask = SIZE - 1;

public:
	inline bool Write(DATA_T value) {
		if (IsFull())
			return false;

		_data[_writeCount++ & _mask] = value;
		return true;
	}

	inline bool Read(DATA_T &value) {
		if (IsEmpty())
			return false;

		value = _data[_readCount++ & _mask];
		return true;
	}

	inline DATA_T First() const {
		return operator [](0);
	}

	inline DATA_T Last() const {
		return operator [](Count());
	}

	inline DATA_T &operator[](INDEX_T i) {
		if (IsEmpty() || i > Count())
			return DATA_T();
		return _data[(_readCount + i) & _mask];
	}

	inline DATA_T operator[](INDEX_T i) const {
		if (IsEmpty())
			return DATA_T();
		return _data[(_readCount + i) & _mask];
	}

	inline bool IsEmpty() const {
		return _writeCount == _readCount;
	}

	inline bool IsFull() const {
		return ((INDEX_T)(_writeCount - _readCount) & (INDEX_T)~(_mask)) != 0;
	}

	INDEX_T Count() const {
		return (_writeCount - _readCount) & _mask;
	}

	inline void Clear() {
		_readCount = _writeCount = 0;
	}

	inline size_t Size() {return SIZE;}
};



class MutexLocker {
public:
	inline explicit MutexLocker(SemaphoreHandle_t mutex) {
		m = mutex;
		xSemaphoreTake(m, portMAX_DELAY);
	}
	inline ~MutexLocker() {
		xSemaphoreGive(m);
	}
	SemaphoreHandle_t m;
};


#define __SPI_GET_FLAG(__HANDLE__, __FLAG__) ((((__HANDLE__)->SR) & (__FLAG__)) == (__FLAG__))

/** @brief  Clear the SPI OVR pending flag.
  * @param  __HANDLE__: specifies the SPI handle.
  *         This parameter can be SPI where x: 1, 2, or 3 to select the SPI peripheral.
  * @retval None
  */
#define __SPI_CLEAR_OVRFLAG(__HANDLE__) do{(__HANDLE__)->DR;\
                                               (__HANDLE__)->SR;}while(0)



char *FloatToString(char *outstr, double value, int places, int minwidth = 0, bool rightjustify = false);
uint8_t GetSignNumbers(double value);
uint8_t GetSignNumbers(int32_t value);

#ifdef __cplusplus
extern "C" {
#endif

void vApplicationTickHook();

#ifdef __cplusplus
}
#endif

#endif /* COMMON_H_ */
