/**
  ******************************************************************************
  * @file    Core/inc/hardware.h
  * @author  Владимир Мешков <vld.meshkov@gmail.com>
  * @version V1.0.0
  * @date    10 декабря 2018
  * @brief   Описание периферии
  ******************************************************************************
  * @attention
  * Всем очко!!
  *
  * <h2><center>&copy; 2018 ООО "РПС"</center></h2>
  ******************************************************************************
  */

#ifndef CORE_INC_HARDWARE_H_
#define CORE_INC_HARDWARE_H_


#include <bitbanding.h>
#include "stm32l4xx_hal.h"

#define LED1_Pin GPIO_PIN_0
#define LED1_GPIO_Port GPIOC
#define LED2_Pin GPIO_PIN_1
#define LED2_GPIO_Port GPIOC
#define LED3_Pin GPIO_PIN_2
#define LED3_GPIO_Port GPIOC
#define PWM_B_Pin GPIO_PIN_0
#define PWM_B_GPIO_Port GPIOA
#define PWM_R_Pin GPIO_PIN_1
#define PWM_R_GPIO_Port GPIOA
#define PWM_G_Pin GPIO_PIN_2
#define PWM_G_GPIO_Port GPIOA
#define M2_Pin GPIO_PIN_3
#define M2_GPIO_Port GPIOA
#define NFC_PD_Pin GPIO_PIN_4
#define NFC_PD_GPIO_Port GPIOA
#define SPI1_NSS_Pin GPIO_PIN_4
#define SPI1_NSS_GPIO_Port GPIOC
#define NFC_IRQ_Pin GPIO_PIN_0
#define NFC_IRQ_GPIO_Port GPIOB
#define BTN2_Pin GPIO_PIN_1
#define BTN2_GPIO_Port GPIOB
#define BTN1_Pin GPIO_PIN_2
#define BTN1_GPIO_Port GPIOB
#define MOTOR_EN_Pin GPIO_PIN_11
#define MOTOR_EN_GPIO_Port GPIOB
#define FPGA_NSS_Pin GPIO_PIN_12
#define FPGA_NSS_GPIO_Port GPIOB
#define BLE_NSS_Pin GPIO_PIN_6
#define BLE_NSS_GPIO_Port GPIOC
#define FPGA_A3_Pin GPIO_PIN_7
#define FPGA_A3_GPIO_Port GPIOC
#define FPGA_A2_Pin GPIO_PIN_8
#define FPGA_A2_GPIO_Port GPIOC
#define FPGA_A1_Pin GPIO_PIN_9
#define FPGA_A1_GPIO_Port GPIOC
#define FPGA_CRST_Pin GPIO_PIN_8
#define FPGA_CRST_GPIO_Port GPIOA
#define FPGA_CDONE_Pin GPIO_PIN_10
#define FPGA_CDONE_GPIO_Port GPIOA
#define FPGA_NRST_Pin GPIO_PIN_15
#define FPGA_NRST_GPIO_Port GPIOA
#define M1_Pin GPIO_PIN_10
#define M1_GPIO_Port GPIOC
#define BLE_RES1_Pin GPIO_PIN_11
#define BLE_RES1_GPIO_Port GPIOC
#define BLE_RES2_Pin GPIO_PIN_3
#define BLE_RES2_GPIO_Port GPIOB
#define BLE_RES3_Pin GPIO_PIN_6
#define BLE_RES3_GPIO_Port GPIOB
#define BLE_RES4_Pin GPIO_PIN_7
#define BLE_RES4_GPIO_Port GPIOB


#define PIN_SET                         0
#define PIN_RESET                       16


#define LED1(p)                         LED1_GPIO_Port->BSRR = LED1_Pin << p
#define LED2(p)                         LED2_GPIO_Port->BSRR = LED2_Pin << p
#define LED3(p)                         LED3_GPIO_Port->BSRR = LED3_Pin << p


#define FPGA_CFG_CRESET(p)              FPGA_CRST_GPIO_Port->BSRR = FPGA_CRST_Pin << p
#define FPGA_SPI_SS(p)                  FPGA_NSS_GPIO_Port->BSRR = FPGA_NSS_Pin << p
#define FPGA_NRST(p)                    FPGA_NRST_GPIO_Port->BSRR = FPGA_NRST_Pin << p

// TODO Заменить BitBanding на что-то другое
#define FPGA_A1_IN						TO_BIT_BAND_PER(FPGA_A1_GPIO_Port->IDR, FPGA_A1_Pin)
#define FPGA_A2_IN						TO_BIT_BAND_PER(FPGA_A2_GPIO_Port->IDR, FPGA_A2_Pin)
#define FPGA_A3_IN                      TO_BIT_BAND_PER(FPGA_A23_GPIO_Port->IDR, FPGA_A3_Pin)

#define MFRC630_PD(p)                   NFC_PD_GPIO_Port->BSRR = NFC_PD_Pin << p
#define MFRC630_NSS(p)                  SPI1_NSS_GPIO_Port->BSRR = SPI1_NSS_Pin << p

#define BLE_SPI_NSS_OUT					TO_BIT_BAND_PER(BLE_NSS_GPIO_Port->ODR, BLE_NSS_Pin)
#define BLE_EN_OUT						TO_BIT_BAND_PER(M1_GPIO_Port->ODR, M1_Pin)

#define SRAM_BB_TEST                    TO_BIT_BAND_SRAM(0x20000300, 2)

#define LED_MAIN_R                      TIM2->CCR2
#define LED_MAIN_G                      TIM2->CCR3
#define LED_MAIN_B                      TIM2->CCR1
#endif /* CORE_INC_HARDWARE_H_ */
