/**
  ******************************************************************************
  * @file    include/spidrv.h
  * @author  Владимир Мешков <vld.meshkov@gmail.com>
  * @version V1.0.0
  * @date    10 ноября 2016
  * @brief   Описание класса SPI
  ******************************************************************************
  * @attention
  * Всем очко!!
  *
  * <h2><center>&copy; 2016 ФГУП "18 ЦНИИ" МО РФ</center></h2>
  ******************************************************************************
  */


#ifndef SPIDRV_H_
#define SPIDRV_H_

#include "stm32l4xx_ll_spi.h"
#include <FreeRTOS.h>
#include <semphr.h>
#include "common.h"


#define SPI_DRV_TIMEOUT					3
#define SPI_DMA_TRANSMIT_TIMEOUT		3


class SPIDrv {
public:
	SPIDrv(SPI_TypeDef *spi);

	SemaphoreHandle_t xSemaphoreIRQTx;
	SemaphoreHandle_t xSemaphoreIRQRx;

	enum DMA {
		DMA_ENABLE,
		DMA_DISABLE
	};

    bool SPI_TransmitOnlyData(const uint8_t *pData, uint16_t Size, DMA dma = DMA_DISABLE, uint32_t Timeout = SPI_DRV_TIMEOUT);
    bool SPI_ReceiveOnlyData(uint8_t *pData, uint16_t Size, DMA dma = DMA_DISABLE, uint32_t Timeout = SPI_DRV_TIMEOUT);
    bool SPI_TransmitReceiveData(uint8_t *pTxData, uint8_t *pRxData, uint16_t Size, DMA dma = DMA_DISABLE, uint32_t Timeout = SPI_DRV_TIMEOUT);

protected:
	SPI_TypeDef *m_pSPI;
	DMA_InitTypeDef m_xDMA_StructureTx;
	DMA_InitTypeDef m_xDMA_StructureRx;

	void InitializeHardware();
	void InitDMA();


private:
	bool WaitOnFlagUntilTimeout(uint32_t Flag, FlagStatus Status, uint32_t Timeout);
};


#endif /* SPIDRV_H_ */
