/**
  ******************************************************************************
  * @file    Core/src/FPGALoader.cpp
  * @author  Владимир Мешков <vld.meshkov@gmail.com>
  * @version V1.0.0
  * @date    10 декабря 2018
  * @brief   Класс конфигурирования FPGA
  ******************************************************************************
  * @attention
  * Всем очко!!
  *
  * <h2><center>&copy; 2018 ООО "РПС"</center></h2>
  ******************************************************************************
  */

#include <SEGGER_RTT.h>
#include <FPGALoader.h>
#include <hardware.h>
#include <stm32l4xx_hal.h>
#include <lz.h>


/// Максимальный размер буфера для LZ77 (257/256 * 1024 + 1), 1024 - размер расжатого буфера
#define LZ_MAX_BUFFER_SIZE          1029
static uint8_t lz_buffer[LZ_MAX_BUFFER_SIZE];


FPGALoader::FPGALoader() {
	m_xQueueCommands = xQueueCreate(1, sizeof(FPGAConfig));
	assert_param(m_xQueueCommands);

	m_xQueueRGB = xQueueCreate(1, sizeof(RGBMessage));
	assert_param(m_xQueueRGB);

	m_xQueueResponce = xQueueCreate(1, sizeof(bool));
	assert_param(m_xQueueResponce);

	m_xSemaphoreIrq = xSemaphoreCreateBinary();
	assert_param(m_xSemaphoreIrq);
	xSemaphoreTake(m_xSemaphoreIrq, 0);

	xMutex = xSemaphoreCreateMutex();
	assert_param(xMutex);

	xEventHandle = xQueueCreateSet(3);
	assert_param(xEventHandle);
	xQueueAddToSet(m_xQueueCommands, xEventHandle);
	xQueueAddToSet(m_xSemaphoreIrq, xEventHandle);
	xQueueAddToSet(m_xQueueRGB, xEventHandle);

    xTaskCreate(task_fpgaloader, "FPGALoader", configMINIMAL_STACK_SIZE * 2, this, tskIDLE_PRIORITY, &handle);
}


void FPGALoader::task() {
    assert_param(m_pSpiDrv);

//    xSemaphoreTake(xMutex, 10);

    _log("Start\n");

    /* Начальное состояние */
    FPGA_NRST(PIN_RESET);
    FPGA_CFG_CRESET(PIN_SET);
    FPGA_SPI_SS(PIN_RESET);
    vTaskDelay(1);

    uint8_t dummy = 0x15;
    bool result;

    RGBMessage rgb;
    FPGAConfig config;
    bool responce;

    uint32_t next_seq = 0;
    int32_t config_bytes_remaining = 0;
uint32_t fpga_a2_last = 0;

    configured = false;
	SetDefaultConfigLz((uint8_t *)0x080FE800);

	configured = false;
	FPGA_SPI_SS(PIN_SET);

    for (;;) {
    	QueueSetMemberHandle_t event = xQueueSelectFromSet(xEventHandle, portMAX_DELAY);
    	if (event == m_xQueueCommands) {
    		xQueueReceive(event, &config, 0);

    		responce = false;
    		if ((config.creset == true) &&
    			 (config.conf_size > 0) &&
				 (config.seq == 0)) {

    			_log("Config reset\n");
    			configured = false;

    			FPGA_NRST(PIN_RESET);
    		    FPGA_CFG_CRESET(PIN_SET);
    		    FPGA_SPI_SS(PIN_RESET);
    		    vTaskDelay(1);

    		    /* Сброс */
    		    FPGA_CFG_CRESET(PIN_RESET);
    		    FPGA_SPI_SS(PIN_RESET);
    		    vTaskDelay(1); // 200 us min

    		    FPGA_CFG_CRESET(PIN_SET);
    		    vTaskDelay(2);
    		    FPGA_SPI_SS(PIN_SET);

    		    uint8_t dummy = 0x15;
    		    m_pSpiDrv->SPI_TransmitOnlyData(&dummy, 1, SPIDrv::DMA_DISABLE, 2);
    		    FPGA_SPI_SS(PIN_RESET);

//    		    _log("seq = 0\n");
//    		    _log("next_seq = 1\n");
    		    next_seq = 1;
    		    config_bytes_remaining = config.conf_size;
//    		    _log("Remaining: %d\n", config_bytes_remaining);

    		    result = m_pSpiDrv->SPI_TransmitOnlyData(config.data.bytes, config.data.size, SPIDrv::DMA_DISABLE, 100);
    		    responce = result;
    		    config_bytes_remaining -= config.data.size;
    		}

    		if ((config.seq == next_seq) && (config_bytes_remaining > 0)) {
//    		    _log("seq = %d\n", config.seq);
//    		    _log("nex_seq = %d\n", next_seq + 1);

    			result = m_pSpiDrv->SPI_TransmitOnlyData(config.data.bytes, config.data.size, SPIDrv::DMA_DISABLE, 100);
    			responce = result;
    			config_bytes_remaining -= config.data.size;
//    			_log("Remaining: %d\n\n", config_bytes_remaining);
    			next_seq += 1;
    		}

            if (config.nreset) {
                _log("Reset\n");
                FPGA_NRST(PIN_RESET);
                vTaskDelay(1);
                FPGA_NRST(PIN_SET);
                responce = true;
            } else {
                FPGA_NRST(PIN_RESET);
            }


    		if (config_bytes_remaining == 0) {
    			FPGA_SPI_SS(PIN_SET);
    			_log("Finish\n");
				for (uint8_t i = 0; i < 20; i++) {
					dummy++;
					result = m_pSpiDrv->SPI_TransmitOnlyData(&dummy, 1, SPIDrv::DMA_DISABLE, 1);
				}
				configured = true;

    		} else if (config_bytes_remaining < 0) {
    			_log("Bytes remaining < 0\n");
    			responce = false;
    		}

    		xQueueSend(m_xQueueResponce, &responce, 1);

    	} else if (event == m_xSemaphoreIrq) {
    		xSemaphoreTake(event, 0);

			bool need_print = false;

			uint32_t tmp;
			tmp = FPGA_A2_IN;
			if (tmp != fpga_a2_last) {
				fpga_a2_last = tmp;
				need_print = true;
			}

			if (need_print) {
				_log("Touch: %d\n\n", fpga_a2_last);
				need_print = false;
			}
//			LED3_OUT = FPGA_A2_IN;

			if (configured) {
				uint8_t fpga_buffer[9] = {0};
				for (uint8_t i = 0; i < 8; i++) {
					fpga_buffer[i] = (i << 1) | 0x01;
				}
				uint8_t fpga_rx_buffer[9] = {0};
				FPGA_SPI_SS(PIN_RESET);
				result = m_pSpiDrv->SPI_TransmitReceiveData(fpga_buffer, fpga_rx_buffer, 9, SPIDrv::DMA_DISABLE, 100);
				FPGA_SPI_SS(PIN_SET);

				if (xSemaphoreTake(xMutex, 10) == pdTRUE) {
					m_xProximity.cs = (fpga_rx_buffer[1] << 8) | fpga_rx_buffer[2];
					m_xProximity.lta = (fpga_rx_buffer[3] << 8) | fpga_rx_buffer[4];
					m_xProximity.byte4 = fpga_rx_buffer[5];
					m_xProximity.byte5 = fpga_rx_buffer[6];
					m_xProximity.counter      = fpga_rx_buffer[8];
					xSemaphoreGive(xMutex);
				}
			}
    	} else if (event == m_xQueueRGB) {
    		xQueueReceive(event, &rgb, 0);
    		_log("RGB\n");

    		uint8_t fpga_tx_buffer[4];
//    		fpga_tx_buffer[0] = 8 << 1;
//    		fpga_tx_buffer[1] = rgb.FPGARed & 0xFF;
//    		fpga_tx_buffer[2] = rgb.FPGAGreen & 0xFF;
//    		fpga_tx_buffer[3] = rgb.FPGABlue & 0xFF;

//    		FPGA_SPI_SS(PIN_RESET);
//    		result = m_pSpiDrv->SPI_TransmitOnlyData(fpga_tx_buffer, 4, SPIDrv::DMA_DISABLE, 100);
//    		FPGA_SPI_SS(PIN_SET);
    		responce = true;
    		xQueueSend(m_xQueueResponce, &responce, 1);
    	} else {
    		__NOP();
    	}
    }
}


bool FPGALoader::GetProximity(Proximity *prox) {
bool ret = false;
	if (prox == NULL or configured == false)
		return false;

	if (xSemaphoreTake(xMutex, 10) == pdTRUE) {
		memcpy(prox, &m_xProximity, sizeof(m_xProximity));
		ret = true;
		xSemaphoreGive(xMutex);
	}
	return ret;
}


void FPGALoader::PrintCommand(const FPGAConfig *config) {
    _log("creset: %s\n", config->creset == true ? "1" : "0");
    _log("nreset: %s\n", config->nreset == true ? "1" : "0");
    _log("conf_size: %d\n", config->conf_size);
    _log("seq: %d\n", config->seq);
    _log("data size: %d\n", config->data.size);
}


void FPGALoader::SetDefaultConfigLz(uint8_t *address) {
bool is_comressed = *(bool *)address;
uint32_t image_size = *(uint32_t *) (address + 1);
uint16_t block_size = *(uint16_t *) (address + 1 + 4);
uint32_t crc32_image = *(uint32_t *) (address + 1 + 4 + 2);
uint16_t blocks_count = image_size / block_size + (image_size % block_size != 0 ? 1 : 0);

    _log("Comressed: %d\n", is_comressed);
    _log("Image size: %d\n", image_size);
    _log("Block_size: %d\n", block_size);
    _log("Blocks count: %d\n", blocks_count);
    _log("CRC32: %08X\n", crc32_image);

    FPGA_NRST(PIN_RESET);
    FPGA_CFG_CRESET(PIN_SET);

    vTaskDelay(1);

    FPGA_CFG_CRESET(PIN_RESET);
    FPGA_SPI_SS(PIN_RESET);
    vTaskDelay(1); // 200 us min

    FPGA_CFG_CRESET(PIN_SET);

    vTaskDelay(2);
    FPGA_SPI_SS(PIN_SET);

uint8_t dummy = 0x15;
    m_pSpiDrv->SPI_TransmitOnlyData(&dummy, 1, SPIDrv::DMA_DISABLE, 2);
    FPGA_SPI_SS(PIN_RESET);

uint8_t *chunk_address = (address + 11);
    for (int block = 0; block < blocks_count; block++) {
        uint16_t chunk_size = *(uint16_t *)(chunk_address);
        uint8_t *chunk_data = (chunk_address + 2);

        unsigned int oos = 0;
        LZ_Uncompress(chunk_data, lz_buffer, chunk_size, &oos);

        m_pSpiDrv->SPI_TransmitOnlyData(lz_buffer, oos, SPIDrv::DMA_DISABLE, 2);
        chunk_address += chunk_size + 2;
    }

    FPGA_SPI_SS(PIN_SET);
    for (uint8_t i = 0; i < 20; i++) {
        dummy++;
        m_pSpiDrv->SPI_TransmitOnlyData(&dummy, 1, SPIDrv::DMA_DISABLE, 1);
    }
    FPGA_NRST(PIN_RESET);

    vTaskDelay(10);
    FPGA_NRST(PIN_SET);

    _log("FPGA Load successfully\n");
}


void EXTI9_5_IRQHandler() {
BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	if (EXTI->PR1 & EXTI_PR1_PIF9) {
		EXTI->PR1 = EXTI_PR1_PIF9;
		xSemaphoreGiveFromISR(FPGALoader::Instance().m_xSemaphoreIrq, &xHigherPriorityTaskWoken);
	}
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}
