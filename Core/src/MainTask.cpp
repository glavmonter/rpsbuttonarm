/**
  ******************************************************************************
  * @file    Core/stc/MainTask.cpp
  * @author  Владимир Мешков <vld.meshkov@gmail.com>
  * @version V1.0.0
  * @date    10 декабря 2018
  * @brief   Центральный Task
  ******************************************************************************
  * @attention
  * Всем очко!!
  *
  * <h2><center>&copy; 2018 ООО "РПС"</center></h2>
  ******************************************************************************
  */

#include <SEGGER_RTT.h>
#include <MainTask.h>
#include <FPGALoader.h>
#include <spidrv.h>
#include <Mifare.h>
#include <ParkPass.h>
#include <uECC.h>
#include <stm32l4xx_ll_rtc.h>

#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"
#include "mbedtls/ecdh.h"
#include "mbedtls/error.h"
#include <pb_encode.h>
#include <pb_decode.h>
#include "rpsproto.pb.h"


extern PCD_HandleTypeDef hpcd_USB_OTG_FS;
extern USBD_HandleTypeDef hUsbDeviceFS;

static void vTimerCallback(TimerHandle_t pxTimer) {
SemaphoreHandle_t semphr = pvTimerGetTimerID(pxTimer);
	xSemaphoreGive(semphr);
}


MainTask::MainTask() {

	m_xSemaphoreUsbRx = xSemaphoreCreateBinary();
	assert_param(m_xSemaphoreUsbRx);
	xSemaphoreTake(m_xSemaphoreUsbRx, 0);

	m_xSemaphoreTimer = xSemaphoreCreateBinary();
	assert_param(m_xSemaphoreTimer);
	xSemaphoreTake(m_xSemaphoreTimer, 0);

	m_xTimer = xTimerCreate("TimerMain", 250, pdTRUE, m_xSemaphoreTimer, vTimerCallback);
	assert_param(m_xTimer);

	xEventHandle = xQueueCreateSet(2 + 2);
	assert_param(xEventHandle);
	xQueueAddToSet(m_xSemaphoreTimer, xEventHandle);
	xQueueAddToSet(m_xSemaphoreUsbRx, xEventHandle);

	xTaskCreate(task_main, "Main", configMINIMAL_STACK_SIZE * 10, this, tskIDLE_PRIORITY, &handle);
}


void MainTask::task() {
    _log("Start\n");
    portENTER_CRITICAL();
        spiDrv = new SPIDrv(SPI2);
        assert_param(spiDrv);

        FPGALoader &fl = FPGALoader::Instance();
        fl.LinkSpiDrv(spiDrv);
        xQueueAddToSet(fl.m_xQueueResponce, xEventHandle);

        Mifare &mifare = Mifare::Instance();
        xQueueAddToSet(mifare.m_xQueueResponce, xEventHandle);

//        ParkPass &pp = ParkPass::Instance();
//        UNUSED(pp);
    portEXIT_CRITICAL();

    vTaskDelay(200);

    MX_USB_DEVICE_Init();
    usb_dev = &hUsbDeviceFS; //reinterpret_cast<USBD_HandleTypeDef *>(hpcd_USB_OTG_FS.pData);

    uint32_t timer_period = USBD_HID_GetPollingInterval(usb_dev);
    xTimerChangePeriod(m_xTimer, timer_period, 0);
    xTimerStart(m_xTimer, 0);


uint32_t cnt = 0;

//	TestECC();

pb_ostream_t nanopb_ostream;
pb_istream_t nanopb_istream;

uint8_t nanopb_buffer[HIDResponce_size + 2];
HIDResponce responce;
FPGAConfig fpga_config;
RGBMessage message_rgb;
BLECommand ble_command;
bool ret = false;
//	BLE_EN_OUT = 1;

    LED_MAIN_R = 0;
    LED_MAIN_G = 0;
    LED_MAIN_B = 0;

    for (;;) {
    	QueueSetMemberHandle_t event = xQueueSelectFromSet(xEventHandle, 1000);
    	if (event == m_xSemaphoreTimer) {
    		xSemaphoreTake(event, 0);
    		responce = HIDResponce_init_zero;
    		ret = fl.GetProximity(&responce.optional_prox.prox);
    		if (ret == true) {
    		    responce.which_optional_prox = HIDResponce_prox_tag;
    		}
	        responce.cnt = cnt++; // Сначала присвоим, потом увеличим

	        ret = mifare.GetMifareCard(responce.optional_mifare_resp.resp.UID.bytes,
	                                  &responce.optional_mifare_resp.resp.UID.size,
	                                  &responce.optional_mifare_resp.resp.ATQA);

	        responce.which_optional_mifare_resp = ret && (responce.optional_mifare_resp.resp.UID.size > 0) ? HIDResponce_resp_tag : 0;

	        nanopb_ostream = pb_ostream_from_buffer(&nanopb_buffer[2], sizeof(nanopb_buffer) - 2);
	        pb_encode(&nanopb_ostream, HIDResponce_fields, &responce);
	        nanopb_buffer[0] = 0x00;
	        nanopb_buffer[1] = nanopb_ostream.bytes_written;
	        assert_param(!(nanopb_ostream.bytes_written > 60));
	        USBD_HID_SendReport(usb_dev, nanopb_buffer, nanopb_ostream.bytes_written + 2);

	        GPIO_PinState btn1 = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_2);
	        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_0, btn1); // BTN1 -> LED1
	        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_1, HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_1)); // BTN2 -> LED2

    	} else if (event == m_xSemaphoreUsbRx) {
    		xSemaphoreTake(event, 0);

    		if (m_vBulkInBuffer[0] == USB_BULK_CMD_FPGA) {
    			nanopb_istream = pb_istream_from_buffer(&m_vBulkInBuffer[2], m_vBulkInBuffer[1]);
    			if (pb_decode(&nanopb_istream, FPGAConfig_fields, &fpga_config) == true) {
    				xQueueSend(FPGALoader::Instance().m_xQueueCommands, &fpga_config, 1);
    			} else {
    				_log("FPGA config err\n");
    				SendBulkResponce(USB_BULK_CMD_FPGA, 0xFD);
    			}

    		} else if (m_vBulkInBuffer[0] == USB_BULK_CMD_RGB) {
    			nanopb_istream = pb_istream_from_buffer(&m_vBulkInBuffer[2], m_vBulkInBuffer[1]);
    			memset(&message_rgb, 0, sizeof(message_rgb));
    			if (pb_decode(&nanopb_istream, RGBMessage_fields, &message_rgb) == true) {
    			    if (message_rgb.which_optional_red == RGBMessage_red_tag)
    				    LED_MAIN_R = message_rgb.optional_red.red;
    				if (message_rgb.which_optional_green == RGBMessage_green_tag)
    				    LED_MAIN_G = message_rgb.optional_green.green;
    				if (message_rgb.which_optional_blue == RGBMessage_blue_tag)
    				    LED_MAIN_B = message_rgb.optional_blue.blue;
    			}
    			SendBulkResponce(USB_BULK_CMD_RGB, 1);

    		} else if (m_vBulkInBuffer[0] == USB_BULK_CMD_BLE) {
    			nanopb_istream = pb_istream_from_buffer(&m_vBulkInBuffer[2], m_vBulkInBuffer[1]);
    			if (pb_decode(&nanopb_istream, BLECommand_fields, &ble_command) == true) {
    			    if (ble_command.which_optional_bEnable == BLECommand_bEnable_tag) {
    			        HAL_GPIO_WritePin(GPIOC, GPIO_PIN_10, ble_command.optional_bEnable.bEnable == true ? GPIO_PIN_SET : GPIO_PIN_RESET);
    			    }
    			}
    			SendBulkResponce(USB_BULK_CMD_BLE, 1);

    		} else if (m_vBulkInBuffer[0] == USB_BULK_CMD_MIFARE) {
    			xQueueSend(Mifare::Instance().m_xQueueCommand, m_vBulkInBuffer, 5);

    		} else if (m_vBulkInBuffer[0] == USB_BULK_CMD_DFU) {
    			LL_RTC_BAK_SetRegister(RTC, LL_RTC_BKP_DR3, 0xCA59);

    			vTaskDelay(1);
    			HAL_RCC_DeInit();
                HAL_DeInit();

                NVIC->ICER[0]=0xFFFFFFFF;
                NVIC->ICPR[0]=0xFFFFFFFF;
                __disable_irq();
    			//vTaskEndScheduler();
    			NVIC_SystemReset();

    		} else {
    			_log("USB bulk error\n");
    			SendBulkResponce(0x00, 0x00);
    		}

    	} else if (event == FPGALoader::Instance().m_xQueueResponce) {
    		bool fpga_responce;
    		xQueueReceive(event, &fpga_responce, 0);
    		SendBulkResponce(USB_BULK_CMD_FPGA, fpga_responce);

    	} else if (event == Mifare::Instance().m_xQueueResponce) {
    		xQueueReceive(event, m_vBulkOutBuffer, 0);
    		USBD_HID_BulkSend(usb_dev, m_vBulkOutBuffer, HID_EP2OUT_SIZE);
    		USBD_LL_PrepareReceive(usb_dev, HID_EP2OUT_ADDR, m_vBulkInBuffer, HID_EP2OUT_SIZE);

    	} else {
    		__NOP();
    	}
    }
}


void MainTask::SendBulkResponce(uint8_t cmd, uint8_t resp) {
	memset(m_vBulkOutBuffer, 0, sizeof(m_vBulkOutBuffer));
	m_vBulkOutBuffer[0] = cmd;
	m_vBulkOutBuffer[1] = 1;
	m_vBulkOutBuffer[2] = resp;
	USBD_HID_BulkSend(usb_dev, m_vBulkOutBuffer, HID_EP2OUT_SIZE);
	USBD_LL_PrepareReceive(usb_dev, HID_EP2OUT_ADDR, m_vBulkInBuffer, HID_EP2OUT_SIZE);
}

int uECC_RNG_Function_Zero(uint8_t *dest, unsigned size) {
	for (unsigned i = 0; i < size; i++)
		dest[i] = rand() & 0xFF;
	return 1;
}


void MainTask::TestECC() {
mbedtls_ecdh_context ctx_srv;
mbedtls_entropy_context entropy;
mbedtls_ctr_drbg_context ctr_drbg;
unsigned char srv_to_cli[32] = {0};
char err_buffer[64];
const char peers[] = "ecdh";
int ret = 1;
const struct uECC_Curve_t *curve = uECC_secp256r1();
uint8_t public_key[65] = {0};
uint8_t public_key_compressed[33] = {0};
uint8_t private_key[32] = {0};
uint8_t shared_key[32] = {0};

	mbedtls_ecdh_init(&ctx_srv);
	mbedtls_ctr_drbg_init(&ctr_drbg);
	mbedtls_entropy_init(&entropy);

	_log("Seeding the random number generator...\n");
	if ((ret = mbedtls_ctr_drbg_seed(&ctr_drbg, mbedtls_entropy_func,
									 &entropy, (const unsigned char *)peers, sizeof(peers))) != 0) {
		mbedtls_strerror(ret, err_buffer, 64);
		_log(" failed: %s\n", err_buffer);
		goto exit_;
	}
	_log("Ok\n");

	_log("Setting up server context...\n");
	ret = mbedtls_ecp_group_load(&ctx_srv.grp, MBEDTLS_ECP_DP_SECP256R1);
	if (ret != 0) {
		mbedtls_strerror(ret, err_buffer, 64);
		_log(" failed: %s\n", err_buffer);
		goto exit_;
	}

	ret = mbedtls_ecdh_gen_public(&ctx_srv.grp, &ctx_srv.d, &ctx_srv.Q,
									mbedtls_ctr_drbg_random, &ctr_drbg);
	if (ret != 0) {
		mbedtls_strerror(ret, err_buffer, 64);
		_log(" failed: %s\n", err_buffer);
		goto exit_;
	}

	// Public key
	ret = mbedtls_mpi_write_binary(&ctx_srv.Q.X, srv_to_cli, sizeof(srv_to_cli));
	if (ret != 0) {
		mbedtls_strerror(ret, err_buffer, 64);
		_log(" failed: %s\n", err_buffer);
		goto exit_;
	}

	_log("OK\n");
	_log("Srv_to_cli:");
	PrintBuffer(srv_to_cli, sizeof(srv_to_cli));

	uECC_set_rng(uECC_RNG_Function_Zero);
	_log("Private Key size: %d\n", uECC_curve_private_key_size(curve));
	_log("Public  Key size: %d\n", uECC_curve_public_key_size(curve));

	ret = uECC_make_key(public_key, private_key, curve);
	_log("Make key: %d\n", ret);
	uECC_compress(public_key, public_key_compressed, curve);

	ret = uECC_shared_secret(srv_to_cli, private_key, shared_key, curve);
	_log("Shared: %d", ret);
	PrintBuffer(shared_key, sizeof(shared_key));

	_log("Read public key\n");
	ret = mbedtls_mpi_lset(&ctx_srv.Qp.Z, 1);
	if (ret != 0) {
		mbedtls_strerror(ret, err_buffer, 64);
		_log(" failed: %s\n", err_buffer);
		goto exit_;
	}

	ret = mbedtls_mpi_read_binary(&ctx_srv.Qp.Z, public_key, 32);
	if (ret != 0) {
		mbedtls_strerror(ret, err_buffer, 64);
		_log(" failed: %s\n", err_buffer);
		goto exit_;
	}

	ret = mbedtls_ecdh_compute_shared(&ctx_srv.grp, &ctx_srv.z,
										&ctx_srv.Qp, &ctx_srv.d,
										mbedtls_ctr_drbg_random, &ctr_drbg);
	if (ret != 0) {
		mbedtls_strerror(ret, err_buffer, 64);
		_log(" failed: %s\n", err_buffer);
		goto exit_;
	}

exit_:
	mbedtls_ecdh_free(&ctx_srv);
	mbedtls_ctr_drbg_free(&ctr_drbg);
}



void MainTask::PrintBuffer(const uint8_t *buffer, uint8_t len) {
	if (len == 0)
		return;

	SEGGER_RTT_printf(0, "\n===================Buffer======================\n");
	for (int str = 0; str < len; str += 16) {
		uint8_t ll = ((len - str) < 16) ? (len - str) : 16;
		for (uint8_t ch = 0; ch < ll; ch++) {
			SEGGER_RTT_printf(0, "%02X ", buffer[str + ch]);
		}
		SEGGER_RTT_printf(0, "\n");
	}
	SEGGER_RTT_printf(0, "===============================================\n");
}


void MainTask::USBReceived() {
BaseType_t xHigherPriorityTaskWoken = pdFALSE;
	xSemaphoreGiveFromISR(m_xSemaphoreUsbRx, &xHigherPriorityTaskWoken);
	portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}


void MainTask::run() {
    vTaskStartScheduler();
}
