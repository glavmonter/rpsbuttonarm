/**
  ******************************************************************************
  * @file    Core/stc/Mifare.cpp
  * @author  Владимир Мешков <vld.meshkov@gmail.com>
  * @version V1.0.0
  * @date    19 декабря 2018
  * @brief   Реализация класса Mifare
  ******************************************************************************
  * @attention
  * Всем очко!!
  *
  * <h2><center>&copy; 2018 ООО "РПС"</center></h2>
  ******************************************************************************
  */

#include <string.h>
#include <SEGGER_RTT.h>
#include <Mifare.h>
#include <mfrc630.h>
#include <stm32l4xx_ll_tim.h>


static void vTimerCallback(TimerHandle_t pxTimer) {
SemaphoreHandle_t semphr = pvTimerGetTimerID(pxTimer);
	xSemaphoreGive(semphr);
}


Mifare::Mifare() {
	m_pSPI = SPI1;

	xMutex = xSemaphoreCreateMutex();
	assert_param(xMutex);

	m_xSemaphoreTimer = xSemaphoreCreateBinary();
	assert_param(m_xSemaphoreTimer);
	xSemaphoreTake(m_xSemaphoreTimer, 0);

	m_xTimer = xTimerCreate("Mifare", 1, pdTRUE, m_xSemaphoreTimer, vTimerCallback);
	assert_param(m_xTimer);

	m_xQueueCommand = xQueueCreate(1, USB_BULK_TRANSFER_SIZE);
	assert_param(m_xQueueCommand);

	m_xQueueResponce = xQueueCreate(1, USB_BULK_TRANSFER_SIZE);
	assert_param(m_xQueueResponce);

	xEventHandle = xQueueCreateSet(1 + 1);
	assert_param(xEventHandle);
	xQueueAddToSet(m_xSemaphoreTimer, xEventHandle);
	xQueueAddToSet(m_xQueueCommand, xEventHandle);

	ConfigureHardware();

	xTaskCreate(task_mifare, "Mifare", configMINIMAL_STACK_SIZE * 3, this, configMAX_PRIORITIES - 1, &handle);
}


void Mifare::task() {
	_log("Start\n");

	SPI_UNSELECT();
	PowerUp();
	vTaskDelay(5);

 	uint8_t ver = ReadReg(MFRC630_REG_VERSION);
	_log("Version: %02X\n", ver);

	const uint8_t buf[] = MFRC630_RECOM_14443A_ID1_106;
	WriteRegs(MFRC630_REG_DRVMOD, buf, sizeof(buf));
	WriteReg(MFRC630_REG_TXAMP, 0x95);  // Уменьшим выходную амплитуду передатчика
	ver = ReadReg(MFRC630_REG_TXAMP);
	_log("TXAMP: %02X\n", ver);

	xTimerChangePeriod(m_xTimer, 100, 0);
	xTimerStart(m_xTimer, 0);

	for (;;) {
		QueueSetMemberHandle_t event = xQueueSelectFromSet(xEventHandle, portMAX_DELAY);
		if (event == m_xSemaphoreTimer) {
			xSemaphoreTake(event, 0);

			if (xSemaphoreTake(xMutex, 100) == pdTRUE) {
				uint16_t atqa = ISO14443A_REQA();
				if (atqa != 0) {
//					SEGGER_RTT_printf(0, "\n\n");

					m_iATQA = atqa;
					uint8_t sak;
					uint8_t uid[MIFARE_MAX_UID_LEN] = {0};
					uint8_t uid_len = ISO14443A_Select(uid, &sak);

					if (uid_len != 0) {
						memcpy(m_vUID, uid, uid_len);
						m_vUID_size = uid_len;

						size_t pos = 0;
						for (uint8_t i = 0; i < uid_len; i++) {
							size_t n = 0;
							snprintf(m_vString + pos, MIFARE_MAX_STRING_LEN - pos - 1, "%02X %n", uid[i], &n);
							pos += n;
						}
						_log("UID of %d bytes (SAK: %X): %s\n", uid_len, sak, m_vString);
					}
				} else {
					m_vUID_size = 0;
				}
				xSemaphoreGive(xMutex);
			}

		} else if (event == m_xQueueCommand) {
			xQueueReceive(event, transfer_buffer, 0);
			memset(&m_xMifareResponce, 0, sizeof(m_xMifareResponce));
			pb_istream_t istream = pb_istream_from_buffer(&transfer_buffer[2], transfer_buffer[1]);
			if (pb_decode(&istream, MifareCommand_fields, &m_xMifareCommand) == true) {
				m_xMifareResponce.eCommand = m_xMifareCommand.eCommand;
				m_xMifareResponce.vUID.size = m_xMifareCommand.vUID.size;
				memcpy(m_xMifareResponce.vUID.bytes, m_xMifareCommand.vUID.bytes, m_xMifareCommand.vUID.size);

				MifareResponce_MFStatus status = MifareResponce_MFStatus_MF_STAT_NOT_IMPLEMENTED;
				switch (m_xMifareCommand.eCommand) {
				case MFCmd_MF_VERIFY_KEY:
					_log("MFCmd_MF_VERIFY_KEY\n");

					if ((m_xMifareCommand.xKey.vKey.size > 0 and m_xMifareCommand.xKey.vKey.size <= 6) and
					    (m_xMifareCommand.vUID.size      > 0 and m_xMifareCommand.vUID.size      <= 7)) {

					    if (m_xMifareCommand.xKey.eType == MifareCommand_MFKey_KeyType_KEY_A) {
					        status = CommandVerifyKey(m_xMifareCommand.vUID.bytes, m_xMifareCommand.vUID.size,
					                                  m_xMifareCommand.xKey.vKey.bytes, NULL, m_xMifareCommand.iBlockNum,
					                                  m_xMifareCommand.iTimeout);
					    }

					    if (m_xMifareCommand.xKey.eType == MifareCommand_MFKey_KeyType_KEY_B) {
					        status = CommandVerifyKey(m_xMifareCommand.vUID.bytes, m_xMifareCommand.vUID.size,
					                                  NULL, m_xMifareCommand.xKey.vKey.bytes, m_xMifareCommand.iBlockNum,
					                                  m_xMifareCommand.iTimeout);
					    }

					} else {
					    status = MifareResponce_MFStatus_MF_STAT_PARAMETERS_ERROR;
					}
					break;

				case MFCmd_MF_READ_BLOCK:
					_log("MFCmd_MF_READ_BLOCK\n");
					if ((m_xMifareCommand.xKey.vKey.size > 0 and m_xMifareCommand.xKey.vKey.size <= 6) and
					    (m_xMifareCommand.vUID.size      > 0 and m_xMifareCommand.vUID.size      <= 7)) {

						m_xMifareResponce.iBlockNum = m_xMifareCommand.iBlockNum;

						if (m_xMifareCommand.xKey.eType == MifareCommand_MFKey_KeyType_KEY_A) {
							status = CommandReadBlock(m_xMifareCommand.vUID.bytes, m_xMifareCommand.vUID.size,
									                  m_xMifareCommand.xKey.vKey.bytes, NULL, m_xMifareCommand.iBlockNum,
													  m_xMifareResponce.vBlockData.bytes, m_xMifareCommand.iTimeout);
						}
						if (m_xMifareCommand.xKey.eType == MifareCommand_MFKey_KeyType_KEY_B) {
							status = CommandReadBlock(m_xMifareCommand.vUID.bytes, m_xMifareCommand.vUID.size,
													  NULL, m_xMifareCommand.xKey.vKey.bytes, m_xMifareCommand.iBlockNum,
													  m_xMifareResponce.vBlockData.bytes, m_xMifareCommand.iTimeout);
						}

						if (status == MifareResponce_MFStatus_MF_STAT_OK) {
//							m_xMifareResponce.has_vBlockData = true;
							m_xMifareResponce.vBlockData.size = 16;
						}

					} else {
						status = MifareResponce_MFStatus_MF_STAT_PARAMETERS_ERROR;
					}
					break;

				case MFCmd_MF_WRITE_BLOCK:
					_log("MFCmd_MF_WRITE_BLOCK\n");
					if ((m_xMifareCommand.xKey.vKey.size > 0 and m_xMifareCommand.xKey.vKey.size <= 6) and
					    (m_xMifareCommand.vUID.size > 0 and m_xMifareCommand.vUID.size <= 7) and
					    (m_xMifareCommand.vBlockData.size == 16)) {

						m_xMifareResponce.iBlockNum = m_xMifareCommand.iBlockNum;

						if (m_xMifareCommand.xKey.eType == MifareCommand_MFKey_KeyType_KEY_A) {
							// Write with keyA
							status = CommandWriteBlock(m_xMifareCommand.vUID.bytes, m_xMifareCommand.vUID.size,
													   m_xMifareCommand.xKey.vKey.bytes, NULL, m_xMifareCommand.iBlockNum,
													   m_xMifareCommand.vBlockData.bytes, m_xMifareCommand.iTimeout);
						}

						if (m_xMifareCommand.xKey.eType == MifareCommand_MFKey_KeyType_KEY_B) {
							// Write with keyB
							status = CommandWriteBlock(m_xMifareCommand.vUID.bytes, m_xMifareCommand.vUID.size,
													   NULL, m_xMifareCommand.xKey.vKey.bytes, m_xMifareCommand.iBlockNum,
													   m_xMifareCommand.vBlockData.bytes, m_xMifareCommand.iTimeout);
						}

					} else {
						status = MifareResponce_MFStatus_MF_STAT_PARAMETERS_ERROR;
					}
					break;

				case MFCmd_MF_VALUE_SET:
				case MFCmd_MF_VALUE_INCREMENT:
				case MFCmd_MF_VALUE_DECREMENT:
				case MFCmd_MF_VALUE_RESTORE:
				case MFCmd_MF_VALUE_TRANSFER:
					status = MifareResponce_MFStatus_MF_STAT_NOT_IMPLEMENTED;
					break;

				default:
					status = MifareResponce_MFStatus_MF_STAT_NOT_IMPLEMENTED;
					break;
				}
				m_xMifareResponce.eStatus = status;

			} else {
				// Все плохо, protocol buffer error
				m_xMifareResponce.eCommand = MFCmd_MF_INVALID;
				m_xMifareResponce.vUID.size = 0;
				m_xMifareResponce.eStatus = MifareResponce_MFStatus_MF_STAT_OK;
			}
			pb_ostream_t ostream = pb_ostream_from_buffer(&transfer_buffer[2], sizeof(transfer_buffer) - 2);
			if (pb_encode(&ostream, MifareResponce_fields, &m_xMifareResponce)) {
				transfer_buffer[0] = USB_BULK_CMD_MIFARE;
				transfer_buffer[1] = ostream.bytes_written;
			} else {
				transfer_buffer[0] = USB_BULK_CMD_MIFARE;
				transfer_buffer[1] = 0;
			}
			xQueueSend(m_xQueueResponce, transfer_buffer, 1);

		} else {
			__NOP();
		}
	}
}


MifareResponce_MFStatus Mifare::CommandVerifyKey(const uint8_t *uid, uint8_t uid_size,
		                                         const uint8_t *keyA, const uint8_t *keyB,
						                         uint8_t block, TickType_t timeout) {
//*************************************************************************

TickType_t stop_time = xTaskGetTickCount() + timeout;
MifareResponce_MFStatus result = MifareResponce_MFStatus_MF_STAT_NOT_IMPLEMENTED;

	if (keyA == NULL && keyB == NULL)
		return MifareResponce_MFStatus_MF_STAT_PARAMETERS_ERROR;

	if (keyA != NULL && keyB != NULL)
		return MifareResponce_MFStatus_MF_STAT_PARAMETERS_ERROR;

	while (xTaskGetTickCount() < stop_time) {
		uint8_t atqa = ISO14443A_REQA();
		if (atqa != 0) {
			_log("Card detected\n");
			uint8_t sak;
			uint8_t l_uid[MIFARE_MAX_UID_LEN] = {0};
			uint8_t l_uid_size = ISO14443A_Select(l_uid, &sak);
			size_t pos = 0;
			for (uint8_t i = 0; i < l_uid_size; i++) {
				size_t n = 0;
				snprintf(m_vString + pos, MIFARE_MAX_STRING_LEN - pos - 1, "%02X %n", l_uid[i], &n);
				pos += n;
			}
			_log("UID of %d bytes (SAK: %X): %s\n", l_uid_size, sak, m_vString);

			if (uid_size != l_uid_size) {
				result = MifareResponce_MFStatus_MF_STAT_UID_NOT_VALID;
				break;
			}

			if (memcmp(uid, l_uid, l_uid_size) != 0) {
				result = MifareResponce_MFStatus_MF_STAT_UID_NOT_VALID;
				break;
			}

			if (keyA != NULL) {
				MFRC630_Cmd_Load_Key(keyA);
				if (MFRC630_MF_Auth(uid, MFRC630_MF_AUTH_KEY_A, block)) {
					_log("KeyA Auth\n");
					MFRC630_MF_DeAuth();
					result = MifareResponce_MFStatus_MF_STAT_OK;
					break;
				} else {
					_log("KeyA NOT Auth\n");
					result = MifareResponce_MFStatus_MF_STAT_PASSWD_ERROR;
					break;
				}
			}

			if (keyB != NULL) {
				MFRC630_Cmd_Load_Key(keyB);
				if (MFRC630_MF_Auth(uid, MFRC630_MF_AUTH_KEY_B, block)) {
					_log("KeyB Auth\n");
					MFRC630_MF_DeAuth();
					result = MifareResponce_MFStatus_MF_STAT_OK;
					break;
				} else {
					_log("KeyB NOT Auth\n");
					result = MifareResponce_MFStatus_MF_STAT_PASSWD_ERROR;
					break;
				}
				break;
			}
		}
		vTaskDelay(5);
	}

	if (xTaskGetTickCount() >= stop_time) {
		_log("Timeout\n");
		return MifareResponce_MFStatus_MF_STAT_TIMEOUT;
	}

	return result;
}

MifareResponce_MFStatus Mifare::CommandReadBlock( const uint8_t *uid, uint8_t uid_size,
		                                          const uint8_t *keyA, const uint8_t *keyB,
						                          uint8_t block, uint8_t *block_data, TickType_t timeout) {
//*************************************************************************
TickType_t stop_time = xTaskGetTickCount() + timeout;
MifareResponce_MFStatus result = MifareResponce_MFStatus_MF_STAT_NOT_IMPLEMENTED;

	if (keyA == NULL && keyB == NULL)
		return MifareResponce_MFStatus_MF_STAT_PARAMETERS_ERROR;

	if (keyA != NULL && keyB != NULL)
		return MifareResponce_MFStatus_MF_STAT_PARAMETERS_ERROR;

	while (xTaskGetTickCount() < stop_time) {
		uint8_t atqa = ISO14443A_REQA();
		if (atqa != 0) {
			_log("Card detected\n");
			uint8_t sak;
			uint8_t l_uid[MIFARE_MAX_UID_LEN] = {0};
			uint8_t l_uid_size = ISO14443A_Select(l_uid, &sak);
			size_t pos = 0;
			for (uint8_t i = 0; i < l_uid_size; i++) {
				size_t n = 0;
				snprintf(m_vString + pos, MIFARE_MAX_STRING_LEN - pos - 1, "%02X %n", l_uid[i], &n);
				pos += n;
			}
			_log("Read block. UID of %d bytes (SAK: %X): %s\n", l_uid_size, sak, m_vString);

			if (uid_size != l_uid_size) {
				result = MifareResponce_MFStatus_MF_STAT_UID_NOT_VALID;
				break;
			}
			if (memcmp(uid, l_uid, l_uid_size) != 0) {
				result = MifareResponce_MFStatus_MF_STAT_UID_NOT_VALID;
				break;
			}

			if (keyA != NULL) {
				MFRC630_Cmd_Load_Key(keyA);
				if (MFRC630_MF_Auth(uid, MFRC630_MF_AUTH_KEY_A, block)) {
					_log("KeyA Auth, Read Block %d\n", block);
					uint8_t readed = MFRC630_MF_Read_Block(block, block_data);
					_log("Readed: %d\n", readed);
					MFRC630_MF_DeAuth();

					if (readed != 16)
						result = MifareResponce_MFStatus_MF_STAT_READ_FAILED;
					else
						result = MifareResponce_MFStatus_MF_STAT_OK;
					break;

				} else {
					_log("KeyA Not Auth at block %d\n", block);
					result = MifareResponce_MFStatus_MF_STAT_PASSWD_ERROR;
					break;
				}
			}

			if (keyB != NULL) {
				MFRC630_Cmd_Load_Key(keyB);
				if (MFRC630_MF_Auth(uid, MFRC630_MF_AUTH_KEY_B, block)) {
					_log("KeyB Auth, Read block %d\n", block);
					uint8_t readed = MFRC630_MF_Read_Block(block, block_data);
					MFRC630_MF_DeAuth();

					if (readed != 16)
						result = MifareResponce_MFStatus_MF_STAT_READ_FAILED;
					else
						result = MifareResponce_MFStatus_MF_STAT_OK;
					break;

				} else {
					_log("KeyB Not Auth at block %d\n", block);
					result = MifareResponce_MFStatus_MF_STAT_PASSWD_ERROR;
					break;
				}
			}
		}
		vTaskDelay(5);
	}

	if (xTaskGetTickCount() >= stop_time) {
		_log("Read block Timeout\n");
		return MifareResponce_MFStatus_MF_STAT_TIMEOUT;
	}

	return result;
}


MifareResponce_MFStatus Mifare::CommandWriteBlock( const uint8_t *uid, uint8_t uid_size,
		                                           const uint8_t *keyA, const uint8_t *keyB,
						                           uint8_t block, uint8_t *block_data, TickType_t timeout) {
//*************************************************************************
TickType_t stop_time = xTaskGetTickCount() + timeout;
MifareResponce_MFStatus result = MifareResponce_MFStatus_MF_STAT_NOT_IMPLEMENTED;

	if (keyA == NULL && keyB == NULL)
		return MifareResponce_MFStatus_MF_STAT_PARAMETERS_ERROR;
	if (keyA != NULL && keyB != NULL)
		return MifareResponce_MFStatus_MF_STAT_PARAMETERS_ERROR;

	while (xTaskGetTickCount() < stop_time) {
		uint8_t atqa = ISO14443A_REQA();
		if (atqa != 0) {
			_log("Card detected\n");

			uint8_t sak;
			uint8_t l_uid[MIFARE_MAX_UID_LEN] = {0};
			uint8_t l_uis_size = ISO14443A_Select(l_uid, &sak);
			size_t pos = 0;
			for (uint8_t i = 0; i < l_uis_size; i++) {
				size_t n = 0;
				snprintf(m_vString + pos, MIFARE_MAX_STRING_LEN - pos - 1, "%02X %n", l_uid[i], &n);
				pos += n;
			}
			_log("Write block. UID of %d bytes (SAK %X): %s\n", l_uis_size, sak, m_vString);

			if (uid_size != l_uis_size) {
				result = MifareResponce_MFStatus_MF_STAT_UID_NOT_VALID;
				break;
			}
			if (memcmp(uid, l_uid, l_uis_size) != 0) {
				result = MifareResponce_MFStatus_MF_STAT_UID_NOT_VALID;
				break;
			}

			if (keyA != NULL) {
				MFRC630_Cmd_Load_Key(keyA);
				if (MFRC630_MF_Auth(uid, MFRC630_MF_AUTH_KEY_A, block)) {
					_log("KeyA Auth, Write block %d\n", block);
					uint8_t writed = MFRC630_MF_Write_Block(block, block_data);
					_log("Writed %d bytes\n", writed);
					MFRC630_MF_DeAuth();

					if (writed != 16) {
						result = MifareResponce_MFStatus_MF_STAT_WRITE_FAILED;
					} else {
						result = MifareResponce_MFStatus_MF_STAT_OK;
					}
					break;

				} else {
					_log("KeyA not Auth at block %d\n", block);
					result = MifareResponce_MFStatus_MF_STAT_PASSWD_ERROR;
					break;
				}
			}

			if (keyB != NULL) {
				MFRC630_Cmd_Load_Key(keyB);
				if (MFRC630_MF_Auth(uid, MFRC630_MF_AUTH_KEY_B, block)) {
					_log("KeyB Auth, Write block %d\n", block);
					uint8_t writed = MFRC630_MF_Write_Block(block, block_data);
					_log("Writed %d bytes\n", writed);
					MFRC630_MF_DeAuth();

					if (writed != 16) {
						result = MifareResponce_MFStatus_MF_STAT_WRITE_FAILED;
					} else {
						result = MifareResponce_MFStatus_MF_STAT_OK;
					}
					break;

				} else {
					_log("KeyB not Auth at block %d\n", block);
					result = MifareResponce_MFStatus_MF_STAT_PASSWD_ERROR;
					break;
				}
			}
		}
		vTaskDelay(5);
	}

	if (xTaskGetTickCount() >= stop_time) {
		_log("Write block Timeout\n");
		return MifareResponce_MFStatus_MF_STAT_TIMEOUT;
	}

	return result;
}


bool Mifare::GetMifareCard(uint8_t *uid, uint8_t *uid_size, uint32_t *atqa) {
bool ret = false;
	if (xSemaphoreTake(xMutex, 10) == pdTRUE) {
		*atqa = m_iATQA;
		*uid_size = m_vUID_size;
		memcpy(uid, m_vUID, m_vUID_size);

		ret = true;
		xSemaphoreGive(xMutex);
	}
	return ret;
}


void Mifare::PrintSector(const uint8_t *uid, uint8_t sector, const uint8_t *keyA, const uint8_t *keyB) {
	assert_param(keyB != NULL);

uint8_t block_start = sector * 4;
uint8_t block_end = block_start + 4;

	MFRC630_Cmd_Load_Key(keyA);
	if (MFRC630_MF_Auth(uid, MFRC630_MF_AUTH_KEY_A, block_start)) {
		uint8_t readbuf[16] = {0};
		uint8_t len;
		snprintf(m_vString, MIFARE_MAX_STRING_LEN, "Sector %2d (0x%02X)\n", sector, sector);
		_log("%s", m_vString);

		for (uint8_t block = block_start; block < block_end; block++) {
			size_t pos = 0;
			size_t n = 0;
			len = MFRC630_MF_Read_Block(block, readbuf);
			snprintf(m_vString, MIFARE_MAX_STRING_LEN, "[%02X] %n", block, &n);
			pos += n;
			if (len == 16) {
				for (uint8_t ch = 0; ch < len; ch++) {
					snprintf(m_vString + pos, MIFARE_MAX_STRING_LEN - pos, "%02X %n", readbuf[ch], &n);
					pos += n;
				}
			} else {
				for (uint8_t ch = 0; ch < 16; ch++) {
					snprintf(m_vString + pos, MIFARE_MAX_STRING_LEN - pos, "XX %n", &n);
					pos += n;
				}
			}
			_log("%s\n", m_vString);
		}
	}
	MFRC630_MF_DeAuth();
}


uint8_t Mifare::MFRC630_MF_Auth(const uint8_t *uid, uint8_t key_type, uint8_t block) {
	// Enable the right interrupts.

	// configure a timeout timer.
	uint8_t timer_for_timeout = 0;  // should match the enabled interupt.

	// According to datashet Interrupt on idle and timer with MFAUTHENT, but lets
	// include ERROR as well.
	WriteReg(MFRC630_REG_IRQ0EN, MFRC630_IRQ0EN_IDLE_IRQEN | MFRC630_IRQ0EN_ERR_IRQEN);
	WriteReg(MFRC630_REG_IRQ1EN, MFRC630_IRQ1EN_TIMER0_IRQEN);  // only trigger on timer for irq1

	// Set timer to 221 kHz clock, start at the end of Tx.
	MFRC630_Timer_Set_Control(timer_for_timeout, MFRC630_TCONTROL_CLK_211KHZ | MFRC630_TCONTROL_START_TX_END);
	// Frame waiting time: FWT = (256 x 16/fc) x 2 FWI
	// FWI defaults to four... so that would mean wait for a maximum of ~ 5ms

	MFRC630_Timer_Set_Reload(timer_for_timeout, 2000);  // 2000 ticks of 5 usec is 10 ms.
	MFRC630_Timer_Set_Value(timer_for_timeout, 2000);

	uint8_t irq1_value = 0;

	MFRC630_Clear_Irq0();  // clear irq0
	MFRC630_Clear_Irq1();  // clear irq1

	// start the authentication procedure.
	MFRC630_Cmd_Auth(key_type, block, uid);

	// block until we are done
	while (!(irq1_value & (1 << timer_for_timeout))) {
		irq1_value = MFRC630_Irq1();
		if (irq1_value & MFRC630_IRQ1_GLOBAL_IRQ) {
			break;  // stop polling irq1 and quit the timeout loop.
		}
	}

	if (irq1_value & (1 << timer_for_timeout)) {
		// this indicates a timeout
		return 0;  // we have no authentication
	}

	// status is always valid, it is set to 0 in case of authentication failure.
	uint8_t status = ReadReg(MFRC630_REG_STATUS);
	return (status & MFRC630_STATUS_CRYPTO1_ON);
}

uint8_t Mifare::MFRC630_MF_Read_Block(uint8_t block_address, uint8_t* dest) {
	MFRC630_Flush_Fifo();

	WriteReg(MFRC630_REG_TXCRCPRESET, MFRC630_RECOM_14443A_CRC | MFRC630_CRC_ON);
	WriteReg(MFRC630_REG_RXCRCCON, MFRC630_RECOM_14443A_CRC | MFRC630_CRC_ON);

	uint8_t send_req[2] = {MFRC630_MF_CMD_READ, block_address};

	// configure a timeout timer.
	uint8_t timer_for_timeout = 0;  // should match the enabled interupt.

	// enable the global IRQ for idle, errors and timer.
	WriteReg(MFRC630_REG_IRQ0EN, MFRC630_IRQ0EN_IDLE_IRQEN | MFRC630_IRQ0EN_ERR_IRQEN);
	WriteReg(MFRC630_REG_IRQ1EN, MFRC630_IRQ1EN_TIMER0_IRQEN);


	// Set timer to 221 kHz clock, start at the end of Tx.
	MFRC630_Timer_Set_Control(timer_for_timeout, MFRC630_TCONTROL_CLK_211KHZ | MFRC630_TCONTROL_START_TX_END);
	// Frame waiting time: FWT = (256 x 16/fc) x 2 FWI
	// FWI defaults to four... so that would mean wait for a maximum of ~ 5ms
	MFRC630_Timer_Set_Reload(timer_for_timeout, 2000);  // 2000 ticks of 5 usec is 10 ms.
	MFRC630_Timer_Set_Value(timer_for_timeout, 2000);

	uint8_t irq1_value = 0;
	uint8_t irq0_value = 0;

	MFRC630_Clear_Irq0();  // clear irq0
	MFRC630_Clear_Irq1();  // clear irq1

	// Go into send, then straight after in receive.
	MFRC630_Cmd_Transceive(send_req, 2);

	// block until we are done
	while (!(irq1_value & (1 << timer_for_timeout))) {
		irq1_value = MFRC630_Irq1();
		if (irq1_value & MFRC630_IRQ1_GLOBAL_IRQ) {
			break;  // stop polling irq1 and quit the timeout loop.
		}
	}
	MFRC630_Cmd_Idle();

	if (irq1_value & (1 << timer_for_timeout)) {
		// this indicates a timeout
		return 0;
	}

	irq0_value = MFRC630_Irq0();
	if (irq0_value & MFRC630_IRQ0_ERR_IRQ) {
		// some error
		return 0;
	}

	// all seems to be well...
	uint8_t buffer_length = MFRC630_Fifo_Length();
	uint8_t rx_len = (buffer_length <= 16) ? buffer_length : 16;
	MFRC630_Read_Fifo(dest, rx_len);
	return rx_len;
}


uint8_t Mifare::MFRC630_MF_Write_Block(uint8_t block_address, const uint8_t *source) {
	MFRC630_Flush_Fifo();
	WriteReg(MFRC630_REG_TXCRCPRESET, MFRC630_RECOM_14443A_CRC | MFRC630_CRC_ON);
	WriteReg(MFRC630_REG_RXCRCCON, MFRC630_RECOM_14443A_CRC | MFRC630_CRC_OFF);

	uint8_t timer_for_timeout = 0;

	WriteReg(MFRC630_REG_IRQ0EN, MFRC630_IRQ0EN_IDLE_IRQEN | MFRC630_IRQ0EN_ERR_IRQEN);
	WriteReg(MFRC630_REG_IRQ1EN, MFRC630_IRQ1EN_TIMER0_IRQEN);

	// Set timer to 221 kHz clock, start at the end of Tx.
	MFRC630_Timer_Set_Control(timer_for_timeout, MFRC630_TCONTROL_CLK_211KHZ | MFRC630_TCONTROL_START_TX_END);
	// Frame waiting time: FWT = (256 x 16/fc) x 2 FWI
	// FWI defaults to four... so that would mean wait for a maximum of ~ 5ms
	MFRC630_Timer_Set_Reload(timer_for_timeout, 2000);  // 2000 ticks of 5 usec is 10 ms.
	MFRC630_Timer_Set_Value(timer_for_timeout, 2000);

	uint8_t irq1_value = 0;
	uint8_t irq0_value = 0;
	uint8_t res;
	uint8_t send_req[2] = {MFRC630_MF_CMD_WRITE, block_address};

	MFRC630_Clear_Irq0();  // clear irq0
	MFRC630_Clear_Irq1();  // clear irq1

	// Go into send, then straight after in receive.
	MFRC630_Cmd_Transceive(send_req, 2);

	// block until we are done
	while (!(irq1_value & (1 << timer_for_timeout))) {
		irq1_value = MFRC630_Irq1();
		if (irq1_value & MFRC630_IRQ1_GLOBAL_IRQ) {
			break;  // stop polling irq1 and quit the timeout loop.
		}
	}
	MFRC630_Cmd_Idle();

	// check if the first stage was successful:
	if (irq1_value & (1 << timer_for_timeout)) {
		// this indicates a timeout
		return 0;
	}
	irq0_value = MFRC630_Irq0();
	if (irq0_value & MFRC630_IRQ0_ERR_IRQ) {
		// some error
		_log("MFRC630_IRQ0_ERR_IRQ\n");
		return 0;
	}
	uint8_t buffer_length = MFRC630_Fifo_Length();
	if (buffer_length != 1) {
		_log("buffer_length != 1\n");
		return 0;
	}
	MFRC630_Read_Fifo(&res, 1);
	if (res != MFRC630_MF_ACK) {
		_log("res != MFRC630_MF_ACK\n");
		return 0;
	}

	MFRC630_Clear_Irq0();  // clear irq0
	MFRC630_Clear_Irq1();  // clear irq1

	// go for the second stage.
	MFRC630_Cmd_Transceive(source, 16);

	// block until we are done
	while (!(irq1_value & (1 << timer_for_timeout))) {
		irq1_value = MFRC630_Irq1();
		if (irq1_value & MFRC630_IRQ1_GLOBAL_IRQ) {
			break;  // stop polling irq1 and quit the timeout loop.
		}
	}

	MFRC630_Cmd_Idle();

	if (irq1_value & (1 << timer_for_timeout)) {
		// this indicates a timeout
		_log("Timeout\n");
		return 0;
	}

	irq0_value = MFRC630_Irq0();
	if (irq0_value & MFRC630_IRQ0_ERR_IRQ) {
		// some error
		_log("IRQ0 Err\n");
		return 0;
	}

	buffer_length = MFRC630_Fifo_Length();
	if (buffer_length != 1) {
		_log("Fifo != 1\n");
		return 0;
	}
	MFRC630_Read_Fifo(&res, 1);
	if (res == MFRC630_MF_ACK) {
		_log("Ok\n");
		return 16;  // second stage was responded with ack! Write successful.
	}

	return 0;
}


void Mifare::MFRC630_Cmd_Auth(uint8_t key_type, uint8_t block_address, const uint8_t *card_uid) {
	MFRC630_Cmd_Idle();
	uint8_t parameters[6] = {key_type, block_address, card_uid[0], card_uid[1], card_uid[2], card_uid[3]};
	MFRC630_Flush_Fifo();
	MFRC630_Write_Fifo(parameters, 6);
	WriteReg(MFRC630_REG_COMMAND, MFRC630_CMD_MFAUTHENT);
}

void Mifare::MFRC630_Cmd_Load_Key(const uint8_t *key) {
	MFRC630_Cmd_Idle();
	MFRC630_Flush_Fifo();
	MFRC630_Write_Fifo(key, 6);
	WriteReg(MFRC630_REG_COMMAND, MFRC630_CMD_LOADKEY);
}



uint8_t Mifare::ISO14443A_Select(uint8_t *uid, uint8_t *sak) {
	MFRC630_Cmd_Idle();
	MFRC630_Flush_Fifo();

	WriteReg(MFRC630_REG_IRQ0EN, MFRC630_IRQ0EN_RX_IRQEN | MFRC630_IRQ0EN_ERR_IRQEN);
	WriteReg(MFRC630_REG_IRQ1EN, MFRC630_IRQ1EN_TIMER0_IRQEN);

	uint8_t timer_for_timeout = 0;

	// Set timer to 221 kHz
	MFRC630_Timer_Set_Control(timer_for_timeout, MFRC630_TCONTROL_CLK_211KHZ | MFRC630_TCONTROL_START_TX_END);

	MFRC630_Timer_Set_Reload(timer_for_timeout, 1000);
	MFRC630_Timer_Set_Value(timer_for_timeout, 1000);

	uint8_t cascade_level;
	for (cascade_level = 1; cascade_level <= 3; cascade_level++) {
		uint8_t cmd = 0;
		uint8_t known_bits = 0;
		uint8_t send_req[7] = {0};
		uint8_t *uid_this_level = &(send_req[2]);
		uint8_t message_length;

		switch (cascade_level) {
		case 1:
			cmd = MFRC630_ISO14443_CAS_LEVEL_1;
			break;
		case 2:
			cmd = MFRC630_ISO14443_CAS_LEVEL_2;
			break;
		case 3:
			cmd = MFRC630_ISO14443_CAS_LEVEL_3;
			break;
		}

		// Disable CRC
		WriteReg(MFRC630_REG_TXCRCPRESET, MFRC630_RECOM_14443A_CRC | MFRC630_CRC_OFF);
		WriteReg(MFRC630_REG_RXCRCCON, MFRC630_RECOM_14443A_CRC | MFRC630_CRC_OFF);

		// Collisions, 32 max
		uint8_t collision_n;
		for (collision_n = 0; collision_n < 32; collision_n++) {
//			_log("CL: %hhd, coll loop: %hhd, kb: %hhd long", cascade_level, collision_n, known_bits);
//			PrintBuffer(uid_this_level, (known_bits + 8 - 1) / 8);

			// Clear interrupts
			MFRC630_Clear_Irq0();
			MFRC630_Clear_Irq1();

			send_req[0] = cmd;
			send_req[1] = 0x20 + known_bits;

			WriteReg(MFRC630_REG_TXDATANUM, (known_bits % 8) | MFRC630_TXDATANUM_DATAEN);

//			uint8_t rxalign = known_bits % 8;
//			_log("Setting rx align to %hhd\n", rxalign);
//			WriteReg(MFRC630_REG_RXBITCTRL, (0 << 7) | (rxalign << 4));


			// then send
			if ((known_bits % 8) == 0) {
				message_length = ((known_bits / 8)) + 2;
			} else {
				message_length = ((known_bits / 8) + 1) + 2;
			}

//			_log("Send:%hhd long: ", message_length);
//			PrintBuffer(send_req, message_length);
			MFRC630_Cmd_Transceive(send_req, message_length);

			// block until we are done
			uint8_t irq1_value = 0;
			while (!(irq1_value & (1 << timer_for_timeout))) {
				irq1_value = MFRC630_Irq1();
				if (irq1_value & MFRC630_IRQ1_GLOBAL_IRQ) {
					break;
				}
			}
			MFRC630_Cmd_Idle();

			// Проверим статусы
			uint8_t irq0 = MFRC630_Irq0();
			uint8_t error = ReadReg(MFRC630_REG_ERROR);
			uint8_t coll = ReadReg(MFRC630_REG_RXCOLL);
//			_log("irq0: %hhX\n", irq0);
//			_log("error: %hhX\n", error);

			uint8_t collision_pos = 0;
			if (irq0 & MFRC630_IRQ0_ERR_IRQ) {
				// Какая-то ошибка есть
				if (error & MFRC630_ERROR_COLLDET) {
					// Коллизия обнаружена
					if (coll & (1 << 7)) {
						collision_pos = coll & (~(1 << 7));
						_log("Collision at %hhX\n", collision_pos);

						// This be a true collision... we have to select either the address
						// with 1 at this position or with zero
						// ISO spec says typically a 1 is added, that would mean:
						// uint8_t selection = 1;

						// However, it makes sense to allow some kind of user input for this, so we use the
						// current value of uid at this position, first index right byte, then shift such
						// that it is in the rightmost position, ten select the last bit only.
						// We cannot compensate for the addition of the cascade tag, so this really
						// only works for the first cascade level, since we only know whether we had
						// a cascade level at the end when the SAK was received.

						uint8_t choice_pos = known_bits + collision_pos;
						uint8_t selection = (uid[((choice_pos + (cascade_level - 1) * 3) / 8)] >> ((choice_pos) % 8)) & 1;

						uid_this_level[((choice_pos)/8)] |= selection << ((choice_pos) % 8);
			            known_bits++;  // add the bit we just decided.

//			            _log("uid_this_level now kb %hhd long: ", known_bits);
//			            PrintBuffer(uid_this_level, 10);
					} else {
						_log("Collision but no valid collpos\n");
						collision_pos = 0x20 - known_bits;
					}
				} else {
					collision_pos = 0x20 - known_bits;
					_log("No collision, error was: %hhX, setting collision_pos to: %hhx\n", error, collision_pos);
				}
			} else if (irq0 & MFRC630_IRQ0_RX_IRQ) {
				// we got data, and no collision, that means all is well
				collision_pos = 0x20 - known_bits;
//				_log("Got data, no collision, setting to: %hhX\n", collision_pos);
			} else {
				// We have no error, nor receive an RX. No responce, no card?
				return 0;
			}

//			_log("Collision_pos: %hhx\n", collision_pos);

			// read the UID Cln so far from the buffer
			uint8_t rx_len = MFRC630_Fifo_Length();
			uint8_t buf[5];

			MFRC630_Read_Fifo(buf, rx_len < 5 ? rx_len : 5);
//			_log("Fifo %hhd long", rx_len);
//			PrintBuffer(buf, rx_len);
//
//			_log("uid_this_level kb %hhd long:", known_bits);
//			PrintBuffer(uid_this_level, (known_bits + 8 - 1) / 8);

			uint8_t rbx;
			for (rbx = 0; rbx < rx_len; rbx++) {
				uid_this_level[(known_bits / 8) + rbx] |= buf[rbx];
			}
			known_bits += collision_pos;
//			_log("known_bits: %hhx\n", known_bits);

			if (known_bits >= 32) {
//				_log("exit collision loop: uid_this_level kb %hhd long", known_bits);
//				PrintBuffer(uid_this_level, 10);
				break;
			}
		} // for (collision_n = 0; collision_n < 32; collision_n++)

		// check if the BCC matches
		uint8_t bcc_val = uid_this_level[4];
		uint8_t bcc_calc = uid_this_level[0]^uid_this_level[1]^uid_this_level[2]^uid_this_level[3];
		if (bcc_val != bcc_calc) {
			_log("Something went wrong, BCC does not match\n");
			return 0;
		}

		MFRC630_Clear_Irq0();
		MFRC630_Clear_Irq1();

		send_req[0] = cmd;
		send_req[1] = 0x70;
		send_req[6] = bcc_calc;
		message_length = 7;

		WriteReg(MFRC630_REG_TXCRCPRESET, MFRC630_RECOM_14443A_CRC | MFRC630_CRC_ON);
		WriteReg(MFRC630_REG_RXCRCCON, MFRC630_RECOM_14443A_CRC | MFRC630_CRC_ON);

		// Reset Tx and Rx registers
		WriteReg(MFRC630_REG_TXDATANUM, (known_bits % 8) | MFRC630_TXDATANUM_DATAEN);
		uint8_t rxalign = 0;
		WriteReg(MFRC630_REG_RXBITCTRL, (0 << 7) | (rxalign << 4));

		// Send it
		MFRC630_Cmd_Transceive(send_req, message_length);
//		_log("send_req %hhd long: ", message_length);
//		PrintBuffer(send_req, message_length);

		uint8_t irq1_value = 0;
		while (!(irq1_value & (1 << timer_for_timeout))) {
			irq1_value = MFRC630_Irq1();
			if (irq1_value & MFRC630_IRQ1_GLOBAL_IRQ) {
				break;
			}
		}
		MFRC630_Cmd_Idle();

		// Check the source of exiting loop
		uint8_t irq0_value = MFRC630_Irq0();
		if (irq0_value & MFRC630_IRQ0_ERR_IRQ) {
			uint8_t error = ReadReg(MFRC630_REG_ERROR);
			if (error & MFRC630_ERROR_COLLDET) {
				return 0;
			}
		}

		// Read the SAK answer from the fifo
		uint8_t sak_len = MFRC630_Fifo_Length();
		if (sak_len != 1) {
			return 0;
		}

		uint8_t sak_value;
		MFRC630_Read_Fifo(&sak_value, sak_len);
//		_log("SAK answer: %hhX\n", sak_value);
		*sak = sak_value;

		if (sak_value & (1 << 2)) {
			uint8_t UIDn;
			for (UIDn = 0; UIDn < 3; UIDn++) {
				// uid_this_level[UIDn] = uid_this_level[UIDn + 1];
				uid[(cascade_level-1)*3 + UIDn] = uid_this_level[UIDn + 1];
			}
		} else {
			// Done according so SAK!
			// Add the bytes at this level to the UID.
			uint8_t UIDn;
			for (UIDn=0; UIDn < 4; UIDn++) {
				uid[(cascade_level - 1) * 3 + UIDn] = uid_this_level[UIDn];
			}
			// Finally, return the length of the UID that's now at the uid pointer.
			return cascade_level * 3 + 1;
		}

//		_log("Exit cascade %hhd long", cascade_level);
//		PrintBuffer(uid, 10);
	}
	return 0;
}


uint16_t Mifare::ISO14443A_REQA() {
	return ISO14443A_WUPA_REQA(MFRC630_ISO14443_CMD_REQA);
}


uint16_t Mifare::ISO14443A_WUPA() {
	return ISO14443A_WUPA_REQA(MFRC630_ISO14443_CMD_WUPA);
}


uint16_t Mifare::ISO14443A_WUPA_REQA(uint8_t instruction) {
	MFRC630_Cmd_Idle();
	MFRC630_Flush_Fifo();

	WriteReg(MFRC630_REG_TXDATANUM, 7 | MFRC630_TXDATANUM_DATAEN);

	// Disable CRC registers
	WriteReg(MFRC630_REG_TXCRCPRESET, MFRC630_RECOM_14443A_CRC | MFRC630_CRC_OFF);
	WriteReg(MFRC630_REG_RXCRCCON, MFRC630_RECOM_14443A_CRC | MFRC630_CRC_OFF);

	WriteReg(MFRC630_REG_RXBITCTRL, 0);

	uint8_t send_req[] = {instruction};

	MFRC630_Clear_Irq0();
	MFRC630_Clear_Irq1();

	// Enable global IRQ for Rx done and Errors
	WriteReg(MFRC630_REG_IRQ0EN, MFRC630_IRQ0EN_RX_IRQEN | MFRC630_IRQ0EN_ERR_IRQEN);
	WriteReg(MFRC630_REG_IRQ1EN, MFRC630_IRQ1EN_TIMER0_IRQEN);

	uint8_t timer_for_timeout = 0;

	// Set timer to 221 kHz clock, start at the end of Tx
	MFRC630_Timer_Set_Control(timer_for_timeout, MFRC630_TCONTROL_CLK_211KHZ | MFRC630_TCONTROL_START_TX_END);
	MFRC630_Timer_Set_Reload(timer_for_timeout, 1000);
	MFRC630_Timer_Set_Value(timer_for_timeout, 1000);

	MFRC630_Cmd_Transceive(send_req, 1);

	uint8_t irq1_value = 0;
	while (!(irq1_value & (1 << timer_for_timeout))) {
		irq1_value = MFRC630_Irq1();
		if (irq1_value & MFRC630_IRQ1_GLOBAL_IRQ) {
			break;
		}
	}

	MFRC630_Cmd_Idle();
	// if no RX IRQ, or if there's an error somehow, return 0
	uint8_t irq0 = MFRC630_Irq0();
	if ((!(irq0 & MFRC630_IRQ0_RX_IRQ)) || (irq0 & MFRC630_IRQ0_ERR_IRQ)) {
		return 0;
	}

	uint8_t rx_len = MFRC630_Fifo_Length();
	uint16_t res;
	if (rx_len == 2) { // ATQA should answer with 2 bytes
		MFRC630_Read_Fifo((uint8_t *)&res, rx_len);
		return res;
	}
	return 0;
}


void Mifare::WriteRegs(uint8_t reg, const uint8_t *values, uint8_t len) {
uint8_t instruction_tx[len + 1];
uint8_t discard[len + 1];
	instruction_tx[0] = (reg << 1) | 0x00;
	for (uint8_t i = 0; i < len; i++) {
		instruction_tx[i + 1] = values[i];
	}

	SPI_SELECT();
		SPI_TransmitReceiveData(instruction_tx, discard, len + 1, DMA_DISABLE, 100);
	SPI_UNSELECT();
}


void Mifare::WriteReg(uint8_t reg, uint8_t value) {
uint8_t instruction_tx[2];
uint8_t discard[2] = {0};
	instruction_tx[0] = (reg << 1) | 0x00;
	instruction_tx[1] = value;

	SPI_SELECT();
		SPI_TransmitReceiveData(instruction_tx, discard, 2, DMA_DISABLE, 100);
	SPI_UNSELECT();
}


uint8_t Mifare::ReadReg(uint8_t reg) {
uint8_t instruction_tx[2];
uint8_t instruction_rx[2] = {0};
	instruction_tx[0] = (reg << 1) | 0x01;
	instruction_tx[1] = 0;

	SPI_SELECT();
		SPI_TransmitReceiveData(instruction_tx, instruction_rx, 2, DMA_DISABLE, 100);
	SPI_UNSELECT();

	return instruction_rx[1];
}


void Mifare::MFRC630_Write_Fifo(const uint8_t *data, uint16_t len) {
uint8_t write_instruction[] = {(MFRC630_REG_FIFODATA << 1)};
uint8_t discard[len + 1];

	SPI_SELECT();
		SPI_TransmitReceiveData(write_instruction, discard, 1, DMA_DISABLE, 100);
		SPI_TransmitReceiveData(data, discard, len, DMA_DISABLE, 100);
	SPI_UNSELECT();
}


void Mifare::MFRC630_Read_Fifo(uint8_t *rx, uint16_t len) {
uint8_t read_instruction[] = {(MFRC630_REG_FIFODATA << 1) | 0x01, (MFRC630_REG_FIFODATA << 1) | 0x01};
uint8_t read_finish[] = {0};
uint8_t discard[2];

	SPI_SELECT();
		SPI_TransmitReceiveData(read_instruction, discard, 1, DMA_DISABLE, 100);
		for (uint16_t i = 0; i < len - 1; i++) {
			SPI_TransmitReceiveData(read_instruction, rx++, 1, DMA_DISABLE, 100);
		}
		SPI_TransmitReceiveData(read_finish, rx++, 1, DMA_DISABLE, 100);
	SPI_UNSELECT();
}


bool Mifare::SPI_TransmitReceiveData(const uint8_t *pTxData, uint8_t *pRxData, uint16_t Size, DMA dma, uint32_t Timeout) {
uint8_t *pTxBuffPtr = const_cast<uint8_t *>(pTxData);
uint8_t *pRxBuffPtr = pRxData;
uint16_t TxXferCount = Size;

	if (TxXferCount == 1) {
		LL_SPI_TransmitData8(m_pSPI, *pTxBuffPtr++);
		TxXferCount--;
	}

	if (TxXferCount == 0) {
		if (WaitOnFlagUntilTimeout(SPI_FLAG_RXNE, RESET, Timeout) != true)
			return false;

		*pRxBuffPtr = LL_SPI_ReceiveData8(m_pSPI);
	} else {
		while (TxXferCount > 0) {
			if (WaitOnFlagUntilTimeout(SPI_FLAG_TXE, RESET, Timeout) != true)
				return false;

			LL_SPI_TransmitData8(m_pSPI, *pTxBuffPtr++);
			TxXferCount--;

			if (WaitOnFlagUntilTimeout(SPI_FLAG_RXNE, RESET, Timeout) != true)
				return false;
			*pRxBuffPtr++ = LL_SPI_ReceiveData8(m_pSPI);
		}
	}

	if (WaitOnFlagUntilTimeout(SPI_FLAG_BSY, SET, Timeout) != true)
		return false;

	return true;
}


bool Mifare::WaitOnFlagUntilTimeout(uint32_t Flag, FlagStatus Status, uint32_t Timeout) {
TickType_t TickStart = xTaskGetTickCount();
	assert_param(Timeout > 0);
	if (Status == RESET) {
		while (__SPI_GET_FLAG(m_pSPI, Flag) == RESET) {
			if (xTaskGetTickCount() - TickStart > Timeout) {
				return false;
			}
		}
	} else {
		while (__SPI_GET_FLAG(m_pSPI, Flag) != RESET) {
			if (xTaskGetTickCount() - TickStart > Timeout) {
				return false;
			}
		}
	}
	return true;
}


void Mifare::ConfigureHardware() {
GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.Pin = GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7;
	GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
	GPIO_InitStructure.Pull = GPIO_PULLDOWN;
	GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
	GPIO_InitStructure.Alternate = GPIO_AF5_SPI1;
	HAL_GPIO_Init(GPIOA, &GPIO_InitStructure);

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_SPI1);

SPI_HandleTypeDef SpiHandle;
    SpiHandle.Instance = SPI1;

    SpiHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
    SpiHandle.Init.Direction = SPI_DIRECTION_2LINES;
    SpiHandle.Init.CLKPhase = SPI_PHASE_1EDGE;
    SpiHandle.Init.CLKPolarity = SPI_POLARITY_LOW;
    SpiHandle.Init.DataSize = SPI_DATASIZE_8BIT;
    SpiHandle.Init.FirstBit = SPI_FIRSTBIT_MSB;
    SpiHandle.Init.TIMode = SPI_TIMODE_DISABLE;
    SpiHandle.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    SpiHandle.Init.CRCPolynomial = 7;
    SpiHandle.Init.NSS = SPI_NSS_SOFT;
    SpiHandle.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
    SpiHandle.Init.Mode = SPI_MODE_MASTER;
    HAL_SPI_Init(&SpiHandle);

    LL_SPI_Enable(SpiHandle.Instance);
}


void Mifare::PrintBuffer(const uint8_t *buffer, uint8_t len) {
	if (len == 0)
		return;

	SEGGER_RTT_printf(0, "\n===================Buffer======================\n");
	for (int str = 0; str < len; str += 16) {
		uint8_t ll = ((len - str) < 16) ? (len - str) : 16;
		for (uint8_t ch = 0; ch < ll; ch++) {
			SEGGER_RTT_printf(0, "%02X ", buffer[str + ch]);
		}
		SEGGER_RTT_printf(0, "\n");
	}
	SEGGER_RTT_printf(0, "===============================================\n");
}

