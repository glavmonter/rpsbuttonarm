/**
  ******************************************************************************
  * @file    Core/stc/ParkPass.cpp
  * @author  Владимир Мешков <vld.meshkov@gmail.com>
  * @version V1.0.0
  * @date    19 декабря 2018
  * @brief   Реализация класса ParkPass
  ******************************************************************************
  * @attention
  * Всем очко!!
  *
  * <h2><center>&copy; 2019 ООО "РПС"</center></h2>
  ******************************************************************************
  */

#include <string.h>
#include <SEGGER_RTT.h>
#include "ParkPass.h"


static void vTimerCallback(TimerHandle_t pxTimer) {
SemaphoreHandle_t semphr = pvTimerGetTimerID(pxTimer);
	xSemaphoreGive(semphr);
}


ParkPass::ParkPass() {

	m_xSemaphoreTimer = xSemaphoreCreateBinary();
	assert_param(m_xSemaphoreTimer);
	xSemaphoreTake(m_xSemaphoreTimer, 0);

	m_xTimer = xTimerCreate("ParkPass", 100, pdTRUE, m_xSemaphoreTimer, vTimerCallback);
	assert_param(m_xTimer);

	m_xQueueCommand = xQueueCreate(1, USB_BULK_TRANSFER_SIZE);
	assert_param(m_xQueueCommand);

	m_xQueueResponce = xQueueCreate(1, USB_BULK_TRANSFER_SIZE);
	assert_param(m_xQueueResponce);

	xEventHandle = xQueueCreateSet(1 + 1);
	assert_param(xEventHandle);

	xQueueAddToSet(m_xTimer, xEventHandle);
	xQueueAddToSet(m_xQueueCommand, xEventHandle);

	xTaskCreate(task_parkpass, "ParkPass", configMINIMAL_STACK_SIZE * 4, this, configMAX_PRIORITIES - 2, &handle);
	assert_param(handle);
}

void ParkPass::task() {
	_log("Start\n");

	for (;;) {
		QueueSetMemberHandle_t event = xQueueSelectFromSet(xEventHandle, portMAX_DELAY);
		if (event == m_xSemaphoreTimer) {
			xSemaphoreTake(event, 0);
			_log("Timer\n");

		} else if (event == m_xQueueCommand) {
			xQueueReceive(event, transfer_buffer, 0);
			memset(&m_xBleResponce, 0, sizeof(m_xBleResponce));
			pb_istream_t istream = pb_istream_from_buffer(&transfer_buffer[2], transfer_buffer[1]);
			if (pb_decode(&istream, BLECommand_fields, &m_xBleCommand)) {
				_log("BLE command decoded\n");
			}

		} else {
			_log("Timeout\n");
		}
	}
}
