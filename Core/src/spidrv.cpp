/**
  ******************************************************************************
  * @file    include/spidrv.cpp
  * @author  Владимир Мешков <vld.meshkov@gmail.com>
  * @version V1.0.0
  * @date    10 ноября 2016
  * @brief   Реализация класса SPI
  ******************************************************************************
  * @attention
  * Всем очко!!
  *
  * <h2><center>&copy; 2016 ФГУП "18 ЦНИИ" МО РФ</center></h2>
  ******************************************************************************
  */

#include "hardware.h"
#include "SEGGER_RTT.h"
#include "spidrv.h"

//static uint8_t iConstTxData __attribute__ ((aligned(4))) = 0x00;

SPIDrv::SPIDrv(SPI_TypeDef *spi) :
	m_pSPI(spi) {

GPIO_InitTypeDef GPIO_InitStructure;
    GPIO_InitStructure.Pin = GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;
    GPIO_InitStructure.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStructure.Pull = GPIO_NOPULL;
    GPIO_InitStructure.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStructure.Alternate = GPIO_AF5_SPI2;
    HAL_GPIO_Init(GPIOB, &GPIO_InitStructure);

    if (m_pSPI == SPI2) {
        LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_SPI2);
    } else {
        assert_param(0);
    }

    SPI_HandleTypeDef SpiHandle;
    SpiHandle.Instance = spi;

    SpiHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
    SpiHandle.Init.Direction = SPI_DIRECTION_2LINES;
    SpiHandle.Init.CLKPhase = SPI_PHASE_2EDGE;
    SpiHandle.Init.CLKPolarity = SPI_POLARITY_HIGH;
    SpiHandle.Init.DataSize = SPI_DATASIZE_8BIT;
    SpiHandle.Init.FirstBit = SPI_FIRSTBIT_MSB;
    SpiHandle.Init.TIMode = SPI_TIMODE_DISABLE;
    SpiHandle.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
    SpiHandle.Init.CRCPolynomial = 7;
    SpiHandle.Init.NSS = SPI_NSS_SOFT;
    SpiHandle.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
    SpiHandle.Init.Mode = SPI_MODE_MASTER;
    HAL_SPI_Init(&SpiHandle);

    LL_SPI_Enable(SpiHandle.Instance);

	xSemaphoreIRQTx = xSemaphoreCreateBinary();
	assert_param(xSemaphoreIRQTx);
	xSemaphoreTake(xSemaphoreIRQTx, 0);

	xSemaphoreIRQRx = xSemaphoreCreateBinary();
	assert_param(xSemaphoreIRQRx);
	xSemaphoreTake(xSemaphoreIRQRx, 0);
}

void SPIDrv::InitializeHardware() {

}


void SPIDrv::InitDMA() {

}


bool SPIDrv::SPI_TransmitOnlyData(const uint8_t *pData, uint16_t Size, DMA dma, uint32_t Timeout) {

    if (dma == DMA_ENABLE) {
        assert_param(0);
        return false;
    }

uint8_t *pTxBuffPtr = const_cast<uint8_t *>(pData);
uint16_t TxXferCount = Size;
    if (TxXferCount == 1) {
        LL_SPI_TransmitData8(m_pSPI, *pTxBuffPtr);
        TxXferCount--;
    }

    while (TxXferCount > 0) {
        if (WaitOnFlagUntilTimeout(SPI_FLAG_TXE, RESET, Timeout) != true)
            return false;
        LL_SPI_TransmitData8(m_pSPI, *pTxBuffPtr++);
        TxXferCount--;
    }

    if (WaitOnFlagUntilTimeout(SPI_FLAG_TXE, RESET, Timeout) != true)
        return false;

    if (WaitOnFlagUntilTimeout(SPI_FLAG_BSY, SET, Timeout) != true)
        return false;

    __SPI_CLEAR_OVRFLAG(m_pSPI);
	return true;
}


bool SPIDrv::SPI_ReceiveOnlyData(uint8_t *pData, uint16_t Size, DMA dma, uint32_t Timeout) {


	return true;
}


bool SPIDrv::SPI_TransmitReceiveData(uint8_t *pTxData, uint8_t *pRxData, uint16_t Size, DMA dma, uint32_t Timeout) {
uint8_t *pTxBuffPtr = const_cast<uint8_t *>(pTxData);
uint8_t *pRxBuffPtr = pRxData;
uint16_t TxXferCount = Size;

	if (TxXferCount == 1) {
	    LL_SPI_TransmitData8(m_pSPI, *pTxBuffPtr++);
		TxXferCount--;
	}

	if (TxXferCount == 0) {
		if (WaitOnFlagUntilTimeout(SPI_FLAG_RXNE, RESET, Timeout) != true)
			return false;

		*pRxBuffPtr = LL_SPI_ReceiveData16(m_pSPI);
	} else {
		while (TxXferCount > 0) {
			if (WaitOnFlagUntilTimeout(SPI_FLAG_TXE, RESET, Timeout) != true)
				return false;

			LL_SPI_TransmitData8(m_pSPI, *pTxBuffPtr++);
			TxXferCount--;

			if (WaitOnFlagUntilTimeout(SPI_FLAG_RXNE, RESET, Timeout) != true)
				return false;
			*pRxBuffPtr++ = LL_SPI_ReceiveData8(m_pSPI);
		}
	}

	if (WaitOnFlagUntilTimeout(SPI_FLAG_BSY, SET, Timeout) != true)
		return false;

	__SPI_CLEAR_OVRFLAG(m_pSPI);
	return true;
}



bool SPIDrv::WaitOnFlagUntilTimeout(uint32_t Flag, FlagStatus Status, uint32_t Timeout) {

TickType_t TickStart = xTaskGetTickCount();

	assert_param(Timeout > 0);

	if (Status == RESET) {
		while (__SPI_GET_FLAG(m_pSPI, Flag) == RESET) {
			if (xTaskGetTickCount() - TickStart > Timeout) {
				return false;
			}
		}
	} else {
		while (__SPI_GET_FLAG(m_pSPI, Flag) != RESET) {
			if (xTaskGetTickCount() - TickStart > Timeout) {
				return false;
			}
		}
	}
	return true;
}

